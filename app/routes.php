<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

/*Route::get('/', function()
{
	return View::make('login');
});
*/
Route::get('/', ['as' => 'login', 'uses' => 'HomeController@getShowLogin']);
Route::post('login', 'AccountController@postSignIn');

Route::get('salir', ['as' => 'salir', 'uses' => 'AccountController@getSignOut']);

Route::get('home', ['as' => 'home', 'uses' => 'HomeController@getShowHome']);
//Route::get('/', ['as' => 'login', 'uses' => 'HomeController@getShowHome']);

//UserController
Route::post('dni', ['as' => 'post-dni', 'uses' => 'UserController@postDni']);
//Route::get('dni', ['as' => 'post-dni', 'uses' => 'UserController@postDni']); //solo para test
//Route::get('categoriaDetalle', ['as' => 'post-detalle-categoria', 'uses' => 'CategoryController@postDetalle']); //solo para test
Route::post('categoriaDetalle', ['as' => 'post-detalle-categoria', 'uses' => 'CategoryController@postDetalle']);
Route::post('cantidadRegistros', ['as' => 'post-catidad-registros', 'uses' => 'RegistroController@postCantidadRegistros']);
//Route::get('cantidadRegistros', ['as' => 'post-catidad-registros', 'uses' => 'RegistroController@postCantidadRegistros']); // test

Route::get('verRegistros', ['as' => 'post-ver-registros', 'uses' => 'RegistroController@postVerRegistro']); // test
Route::post('verRegistros', ['as' => 'post-ver-registros', 'uses' => 'RegistroController@postVerRegistro']); 

Route::get('ingresarRegistro', ['as' => 'post-ingresar-registro', 'uses' => 'RegistroController@postIngresarRegistro']); // test
Route::post('ingresarRegistro', ['as' => 'post-ingresar-registro', 'uses' => 'RegistroController@postIngresarRegistro']); 

Route::get('moverRegistro', ['as' => 'post-mover-registro', 'uses' => 'RegistroController@postMoverRegistro']); // test
Route::post('moverRegistro', ['as' => 'post-mover-registro', 'uses' => 'RegistroController@postMoverRegistro']); 

Route::get('verificarGrupo', ['as' => 'post-verificar-grupo', 'uses' => 'RegistroController@postVerificarGrupo']); // test
Route::post('verificarGrupo', ['as' => 'post-verificar-grupo', 'uses' => 'RegistroController@postVerificarGrupo']); 

Route::get('eliminarRegistro', ['as' => 'post-mover-registro', 'uses' => 'RegistroController@postEliminarRegistro']); // test
Route::post('eliminarRegistro', ['as' => 'post-mover-registro', 'uses' => 'RegistroController@postEliminarRegistro']); 


Route::get('reporteExcel', ['as' => 'post-reporte-excel', 'uses' => 'ReporteController@reporteExcel']); // test
Route::post('reporteExcel', ['as' => 'post-reporte-excel', 'uses' => 'ReporteController@reporteExcel']); // test
//llave

Route::get('validarLlave', ['as' => 'post-validar-llave', 'uses' => 'KeyController@postValidar']); // test
Route::post('validarLlave', ['as' => 'post-validar-llave', 'uses' => 'KeyController@postValidar']); 

//vistas Administrador
Route::get('crearUsuario', ['as' => 'get-crear-usuario', 'uses' => 'UserController@getCrearUsuario']); // test

Route::get('panel', ['as' => 'get-panel', 'uses' => 'UserController@getPanel']); // test
Route::get('verUsuario', ['as' => 'post-ver-Usuario', 'uses' => 'UserController@postVerUsuario']); // test
Route::post('verUsuario', ['as' => 'post-ver-Usuario', 'uses' => 'UserController@postVerUsuario']); 

Route::get('llave', ['as' => 'get-llave', 'uses' => 'KeyController@getLlave']); 

Route::get('configuracion', ['as' => 'get-configuracion', 'uses' => 'ConfigController@getConfig']); 

Route::get('validarEmail', ['as' => 'post-email', 'uses' => 'UserController@postEmail']); //test
Route::post('validarEmail', ['as' => 'post-email', 'uses' => 'UserController@postEmail']); 

Route::get('ingresarUsuario', ['as' => 'post-ingresar-usuario', 'uses' => 'UserController@postIngresarUsuario']); //test
Route::post('ingresarUsuario', ['as' => 'post-ingresar-usuario', 'uses' => 'UserController@postIngresarUsuario']); 

Route::get('activarUsuario/{id}', ['as' => 'get-activar-usuario', 'uses' => 'UserController@getActivarUsuario']); 
Route::get('eliminarUsuario/{id}', ['as' => 'get-eliminar-usuario', 'uses' => 'UserController@getEliminarUsuario']); 
Route::get('desactivarUsuario/{id}', ['as' => 'get-desactivar-usuario', 'uses' => 'UserController@getDesactivarUsuario']); 

Route::get('crearCuenta', ['as' => 'get-crear-cuenta', 'uses' => 'UserController@getCrearCuenta']); 
Route::get('perfil', ['as' => 'get-perfil', 'uses' => 'UserController@getPerfil']); 







//Route::post('home', 'HomeController@postSignIn');

//Grupo autenticado
Route::group(array('before' => 'auth'), function(){
	//Proteccion de peticiones post
	Route::group(array('before' => 'csrf'),function(){
	});

	
});

