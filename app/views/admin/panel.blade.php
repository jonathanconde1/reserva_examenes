@extends('layouts.backend.base')
@section('content')
@if((Auth::check())&&(Auth::user()->type=='0'))

<link href="js/calendar.min.css" rel="stylesheet">
<link href="css/calendar.min.css" rel="stylesheet">
<link href="css/style.typeahead.css" rel="stylesheet">
<script src="js/calendar.min.js"></script>
<script src="js/typeahead.bundle.js"></script>
<script src="js/jquery.admin.js"></script>



<!-- column 2 --> 
<div class="container col col-md-12">
  <h3 >SUB DIRECCIÓN DE LICENCIAS DE CONDUCIR</h3>  
  <input id="data-fecha" type="hidden" value="">
  <input id="data-operacion" type="hidden" value="">
  <hr>
	   <div class="row">
      <div class="col-md-12">
        
        <div id="formulario" >
          <div class="table">
            <div class="row">
                            
          <div class="col col-md-12" >
            <div class="panel panel-primary" id="tabla_usuarios">
              <div class="panel-heading">
                <div class="panel-title">
                  <i class="glyphicon glyphicon-wrench pull-right"></i>
                  <h4>RELACIÓN DE USUARIOS ACTUALES</h4>

                </div>
              </div>
              
              <div class="panel-body">
                <table class="table" id="data_registro">

                  <tr>
                    <b><div id="fecha"></div></b>
                  </tr>
                  <tr>
                    <th>N°</th>
                    <th>DNI</th>
                    <th>Apellidos y Nombres</th>
                    <th>Email</th>
                    <th>ROL</th>
                    <th>Trámite</th>
                    <th>Estado</th>
                    <th><a>Op.</a></th>
                  </tr>
                  
                  
                </table>
              <div class="control">
                <label></label>
                <center>
                <div style="display:none;">
                <input class="crear btn btn-default btn-success" type="button" value="Crear Usuario"></input>
                <input class="eliminar btn btn-default btn-danger" type="button" value="Eliminar Usuario"></input>
                <input type="button" value="Exportar a Excel" class="btn btn-default btn-success"></input>
                </div>
                </center>
              </div>
              </div>

            </div>
            </div>
           </div><!--end row-->
           
            
</div> <!--end class container-->
@else
Es un error de session o la zona esta restringida para este usuario!
 <a href="{{ URL::route('salir') }}"><input class="btn btn-default btn-success" type="button" value="Iniciar Sesión"></a>
@endif

@stop