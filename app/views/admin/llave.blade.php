@extends('layouts.backend.base')
@section('content')
@if((Auth::check())&&(Auth::user()->type=='0'))
<link href="js/calendar.min.css" rel="stylesheet">
<link href="css/calendar.min.css" rel="stylesheet">
<link href="css/style.typeahead.css" rel="stylesheet">
<script src="js/calendar.min.js"></script>
<script src="js/typeahead.bundle.js"></script>
<script src="js/jquery.admin.js"></script>



<!-- column 2 --> 
<div class="container col col-md-12">
  <h3 >SUB DIRECCIÓN DE LICENCIAS DE CONDUCIR</h3>  
  <input id="data-fecha" type="hidden" value="">
  <input id="data-operacion" type="hidden" value="">
  <hr>
	   <div class="row">
      <div class="col-md-12">
        
        <div id="formulario" >
          <div class="table">
            <div class="row">
                            
          <div class="col col-md-8" >
            <div class="panel panel-primary" id="tabla_usuarios">
              <div class="panel-heading">
                <div class="panel-title">
                  <i class="glyphicon glyphicon-wrench pull-right"></i>
                  <h4>RELACIÓN DE LLAVES</h4>

                </div>
              </div>
              <div class="panel-body">
                <table class="table" id="data_llave">
                  <tr>
                    <b><div id="fecha"></div></b>
                  </tr>
                  <tr>
                    <th>N°</th>
                    <th>CÓDIGO</th>
                    <th>ESTADO</th>
                  </tr>
                  <?php $i = 1;?>
                  @if(isset($keys))
                  @foreach($keys as $key)
                    <tr>
                      <td>{{$i}}</td>
                      <td>{{$key->code}}</td>
                      <td>Activo</td>
                    </tr>
                    <?php $i++;?>
                  @endforeach
                  @endif
                </table>
              	<div class="control">
	                <center>
	                <!--input class="generar btn btn-default btn-success" type="button" value="Generar Llaves"></input-->
	                <input class="btn btn-default btn-success" type="button" value="Exportar a Excel" ></input>

	                </center>
            	  </div><!--end class control-->
             </div><!--end panel body-->
            </div><!--end panel primary-->
          </div><!--end col-md-5-->

          <div id="resultados" class="col col-md-4" >
            <div class="panel panel-primary">
              <div class="panel-heading">
                <div class="panel-title">
                  <i class="glyphicon glyphicon-wrench pull-right"></i>
                  <h4>Consejos</h4>
                </div>
              </div>
              <div class="panel-body">
              <p><b>Llave:</b> Las llaves sirven para ingresar registro extras.</p>
              <p><b>Generación:</b> Ud. siempre podra disponer de la cantidad de llaves establecida
              en la configuracion ya que se generan automaticamente al ingresar a esta vista.</p>
              <p><b>Uso:</b> Copie el código de la llave y peguelo en el formulario que corresponda.</p>
              <p><b>Tiempo de Vida:</b> Solo sirven para un uso.</p>

               

              </div>
              <div id="datos"></div>
            </div>
          </div>

        </div><!--end row-->

</div> <!--end class container-->
@else
Es un error de session o la zona esta restringida para este usuario!
 <a href="{{ URL::route('salir') }}"><input class="btn btn-default btn-success" type="button" value="Iniciar Sesión"></a>
@endif

@stop