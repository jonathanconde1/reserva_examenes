@extends('layouts.backend.base')
@section('content')
@if((Auth::check())&&(Auth::user()->type=='0'))
<link href="js/calendar.min.css" rel="stylesheet">
<link href="css/calendar.min.css" rel="stylesheet">
<link href="css/style.typeahead.css" rel="stylesheet">
<script src="js/calendar.min.js"></script>
<script src="js/typeahead.bundle.js"></script>
<script src="js/jquery.admin.js"></script>



<!-- column 2 --> 
<div class="container col col-md-12">
  <h3 >SUB DIRECCIÓN DE LICENCIAS DE CONDUCIR</h3>  
  <input id="data-fecha" type="hidden" value="">
  <input id="data-operacion" type="hidden" value="">
  <hr>
	   <div class="row">
      <div class="col-md-12">
        
        <div id="formulario" >
          <div class="table">
            <div class="row">
                            
          <div id="add_reg" class="col col-md-8" >
            <div class="panel panel-primary">
              <div class="panel-heading">
                <div class="panel-title">
                  <i class="glyphicon glyphicon-wrench pull-right"></i>
                  <h4>Configuracion Global</h4>
                </div>
              </div>
              <div class="panel-body">
                 <table class='table' >
                  <tr>
                    <td>
                      <label>Cant. registros</label>
                    </td>
                    <td id="cantReg">
                      <div class="controls">
                        <input id="data-cantReg" type="text" class="form-control" placeholder="Cantidad de Registros" value="{{Configuracion::find('1')->max}}" disabled>
                      </div>
                    </td>
                    
                  </tr><!--end row-->
                  <tr>
                    <td>
                      <label>Cant. llaves</label>
                    </td>
                    <td id="CantLlave">
                      <div class="controls">
                        <input id="data-cantLlave" type="text" class="form-control" placeholder="Cantidad de Llaves" value="20" disabled>
                      </div>
                    </td>
                    
                  </tr><!--end row-->
                  
                </table>
                
                <label></label>
                <div class="controls">
                  <center>
                  <input id="agregarRegistro" class="agregar_registro btn btn-default btn-success" type="button" value="Actualizar Configuración" ></input>
                  </center>
                </div>
              </div><!--end add_reg-->
              </div><!--end panel body-->

            </div><!--end panel col -md-8-->

          <div id="resultados" class="col col-md-4" >
            <div class="panel panel-primary">
              <div class="panel-heading">
                <div class="panel-title">
                  <i class="glyphicon glyphicon-wrench pull-right"></i>
                  <h4>Consejos</h4>
                </div>
              </div>
              <div class="panel-body">
              <p><b>Cant. Registros:</b>Es la cantidad de registros
              máximos permitidos por día</p>
              
               <p><b>Cant. Llaves:</b>Es la cantidad de llaves que se generan al ingresar a la
               vista.</p>
               
              </div>
              <div id="datos"></div>
            </div>
          </div>

        </div><!--end row-->

</div> <!--end class container-->
@else
Es un error de session o la zona esta restringida para este usuario!
 <a href="{{ URL::route('salir') }}"><input class="btn btn-default btn-success" type="button" value="Iniciar Sesión"></a>
@endif

@stop