@extends('layouts.base')
@section('content')
@if(Auth::check())

<link href="js/calendar.min.css" rel="stylesheet">
<link href="css/calendar.min.css" rel="stylesheet">
<link href="css/style.typeahead.css" rel="stylesheet">
<script src="js/calendar.min.js"></script>
<script src="js/typeahead.bundle.js"></script>
<script src="js/jquery.controladorEventos.js"></script>



<!-- column 2 --> 
<div class="container col col-md-12">
  <h3 >SUB DIRECCIÓN DE LICENCIAS DE CONDUCIR </h3>
  <div style="display:none;">  
  <input id="data-fecha" type="text" value="">
  <input id="data-fecha-id" type="text" value="">
  <input id="data-operacion" type="hidden" value="">
  </div>
  <hr>
	   <div class="row">
      <div class="col-md-12">
        <div id="calendario" >
          <div class="calendar" data-color="normal">
            
          </div>
        </div>
        <div id="formulario" style="display:none;">
          <div class="table">
            <div class="row">
              <div class="col-12">

               
          <div class="col col-md-12" >
            <div class="panel panel-primary">
              <div class="panel-heading">
                <div class="panel-title">
                  <i class="glyphicon glyphicon-wrench pull-right"></i>
                  <center><h4 id="categoriaDefault"></h4></center>

                  <input id="categoriaActual" type="hidden" value="{{$categoriaDefault}}">


                </div>
              </div>
              
              <div class="panel-body">
                <table class="table" id="data_registro">

                  <tr>
                    <b><div id="fecha"></div></b>
                  </tr>
                  <tr>
                    <th>N°</th>
                    <th>DNI</th>
                    <th>Apellidos y Nombres</th>
                    <th>CAT</th>
                    <th>TIPO</th>
                    <th>Trámite</th>
                    <th>A</th>
                    <th>D</th>
                    <th>NSP</th>
                    <th>Observación</th>
                    <th><a>Op.</a></th>
                  </tr>
                  
                  
                </table>
              <div class="control">
                <label></label>
                <center>
                <input class="cancelar btn btn-default btn-success" type="button" value="Regresar a Calendario"></input>
                <input class="mover btn btn-default btn-danger" type="button" value="Mover Usuario"></input>
                <input class="eliminar btn btn-default btn-danger" type="button" value="Eliminar Usuario"></input>
                <input type="button" value="Exportar a Excel" class="exportar btn btn-default btn-success"></input>

                </center>
              </div>
              </div>

            </div>
            <div id="add_reg" class="col col-md-8" >
            <div class="panel panel-primary">
              <div class="panel-heading">
                <div class="panel-title">
                  <i class="glyphicon glyphicon-wrench pull-right"></i>
                  <h4>Nuevo Registro</h4>
                </div>
              </div>
              <div class="panel-body">
                 <table class='table' >
                  <tr>
                    <td>
                      <label>DNI</label>
                      <div  id="the-basics" >
                        <input id="data-dni" type="text" class="typeahead form-control" placeholder="DNI" size=4>
                        <input id="data-persona_id" type="hidden" value="">
                      </div>      
                    </td>
                    <td id="apellidos">
                      <label>Apellidos</label>
                      <div class="controls">
                        <input id="data-apellidos" type="text" class="form-control" placeholder="Apellidos">
                      </div>
                    </td>
                    <td id="nombres" >
                      <label>Nombres</label>
                      <div class="controls">
                        <input id="data-nombres" type="text" class="form-control" placeholder="Nombres">
                      </div>
                    </td>

                    <!--end row-->
                  </tr>
                  <tr>
                    <td>
                      <label >CATEGORIA</label>
                      <div class="controls">
                        <select id="categoria" class="form-control" >
                          
                        </select>
                      </div>
                    </td>
                    <td>
                      <?php
                      $tipos = Tipo::all();
                      ?>
                      <label>TIPO</label>
                      <div class="controls">
                        <select id="opcion_tipo" class="form-control">
                          @foreach($tipos as $tipo)
                          <option value="{{$tipo->id}}" >{{$tipo->name}}</option>
                          @endforeach
                        </select>
                        
                      </div>
                    </td>
                    <td>
                      <label>Trámite</label>
                      <div class="controls">
                        <input value="{{Auth::user()->tramite}}" type="text" class="form-control" placeholder="Trámite" disabled>
                      </div>
                    </td>
                  </tr>
                </table>
                <div class="col col-md-12">
                  <div id="llave" style="display:none;">
                    <label>LLAVE :</label>
                      <div class="controls">
                        <input id="data-code" type="text" class="form-control" placeholder="Llave"><input class="btn btn-default btn-success" id="verificarCode" type="button" value="Verificar Llave">
                      <label id="codeResultado"></label>
                      </div>
                      
                  </div>
                  <label>Observación</label>
                  <textarea id="data-observacion"class="form-control"></textarea>
                </div>
                <label></label>
                <div class="controls">
                  <center>
                  <input id="agregarRegistro" class="agregar_registro btn btn-default btn-success" type="button" value="Agregar Registro" ></input>
                  </center>
                </div>
              </div><!--end add_reg-->
              </div><!--end panel body-->

            </div><!--end panel col -md-8-->
                        
            <div id="resultados" class="col col-md-4" >
            <div class="panel panel-primary">
              <div class="panel-heading">
                <div class="panel-title">
                  <i class="glyphicon glyphicon-wrench pull-right"></i>
                  <h4>Consejos</h4>
                </div>
              </div>
              <div class="panel-body">
              <p>Ingrese el DNI para buscar los datos de la persona.</p>
              <p>En caso que no se encuentre la persona deberá ingresar por unica vez los
               Apellidos y Nombres del postulante</p>

              </div>
              <div id="datos"></div>
            </div>
          </div>

          <form action="reporteExcel" method="POST" id="exportar" style="display:none;">
            <input id="exporta_dia_id" type="text" name="dia_id">
            <input id="exporta_dia" type="text" name="dia">
            <input id="exporta_mes" type="text" name="mes">
            <input id="exporta_anio" type="text" name="anio">
            <input id="exporta_categoria_id" type="text" name="categoria_id">
          </form>

            </div>
              
              </div>

            </div>
            
          </div>
          
          Requerimientos:<br>
          //el orden importa<br>
          


        </div>
        
     </div>
</div> <!--end class container-->
@else
Error de sesión  ...!<br>
<a href="{{ URL::route('salir') }}"><input class="btn btn-default btn-success" type="button" value="Iniciar Sesión"></a>
  
@endif

@stop
