@extends('layouts.backend.base')
@section('content')

<link href="js/calendar.min.css" rel="stylesheet">
<link href="css/calendar.min.css" rel="stylesheet">
<link href="css/style.typeahead.css" rel="stylesheet">
<script src="js/calendar.min.js"></script>
<script src="js/typeahead.bundle.js"></script>
<script src="js/jquery.controladorEventos.js"></script>



<!-- column 2 --> 
<div class="container col col-md-12">
  <h3 >SUB DIRECCIÓN DE LICENCIAS DE CONDUCIR</h3>  
  <input id="data-fecha" type="hidden" value="">
  <input id="data-operacion" type="hidden" value="">
  <hr>
	   <div class="row">
      <div class="col-md-12">
        
        <div id="formulario" >
          <div class="table">
          
           <div class="row" id="nuevo_usuario">
           	<div id="add_reg" class="col col-md-8" >
            <div class="panel panel-primary">
              <div class="panel-heading">
                <div class="panel-title">
                  <i class="glyphicon glyphicon-wrench pull-right"></i>
                  <h4>Nuevo Usuario</h4>
                </div>
              </div>
              <div class="panel-body">
                 <table class='table' >
                  <tr>
                    <td>
                      <label>DNI</label>
                      <div  id="the-basics" >
                        <input id="data-dni" type="text" class="typeahead form-control" placeholder="DNI" size=4>
                        <input id="data-persona_id" type="hidden" value="">
                      </div>      
                    </td>
                    <td id="apellidos">
                      <label>Apellidos</label>
                      <div class="controls">
                        <input id="data-apellidos" type="text" class="form-control" placeholder="Apellidos">
                      </div>
                    </td>
                    <td id="nombres" >
                      <label>Nombres</label>
                      <div class="controls">
                        <input id="data-nombres" type="text" class="form-control" placeholder="Nombres">
                      </div>
                    </td>

                  </tr><!--end row-->
                  <tr>
                  	<td id="email" >
                      <label>Email</label>
                      <div class="controls">
                        <input id="data-email" type="text" class="form-control" placeholder="Email" data-value="0">
                        
                      </div>
                    </td>
                    <td>
                      <label >Rol</label>
                      <div class="controls">
                        <select id="data-rol" class="form-control" >
                          <option value="1" >Digitador</option>
                          <option value="0" >Administrador</option>

                        </select>
                      </div>
                    </td>
                    <td>
                      <label>Trámite</label>
                      <div class="controls">
                        <input id="data-tramite" type="text" class="form-control" placeholder="Trámite" >
                      </div>
                    </td>
                  </tr>
                  <tr>
                    <td id="celular" >
                      <label>Celular</label>
                      <div class="controls">
                        <input id="data-celular" type="text" class="form-control" placeholder="Celular">
                      </div>
                    </td>
                    <td>
                      <label >Password</label>
                      <div class="controls">
                        <input id="data-pass" type="password" class="form-control" placeholder="password" >
                      </div>
                    </td>
                    <td>
                      <label></label>
                      <div id="info_email"></div>
                    </td>
                  </tr>
                </table>
               
                <div class="controls">
                  <center>
                  <a href="salir"><button id="cancelarUsuario" class="btn btn-default btn-success" type="button" value="Cancelar Registro" >Cancelar Registro</button></a>
                  <input id="agregarUsuario" class="agregar_registro btn btn-default btn-success" type="button" value="Agregar Registro" ></input>
                  </center>
                </div>
              </div><!--end add_reg-->
              </div><!--end panel body-->

            </div><!--end panel col -md-8-->
                        
            <div id="resultados" class="col col-md-4" >
            <div class="panel panel-primary">
              <div class="panel-heading">
                <div class="panel-title">
                  <i class="glyphicon glyphicon-wrench pull-right"></i>
                  <h4>Consejos</h4>
                </div>
              </div>
              <div class="panel-body">
              <p><b>DNI:</b> Ingrese el DNI para buscar los datos de la persona.</p>
              <p>En caso que no se encuentre la persona deberá ingresar por unica vez los
               Apellidos y Nombres</p>
               <p><b>Password:</b> Definir la contraseña para este usuario.</p>
               <p>Se recomienda cambiar mensualmente el password.</p>
               <p id="datos"></p>
              </div>
              
              <div >
                Aqui los enlaces para retornar
                <a id="retornar" href="salir"><button>Retornar</button></a>
              </div>

            </div>
          </div>


           </div>
            


            </div>
              
              </div>

            </div>
            
          </div>
       
        </div>
        
     </div>
</div> <!--end class container-->

@stop