@extends('layouts.backend.base')
@section('content')
@if(Auth::check())
	

<link href="js/calendar.min.css" rel="stylesheet">
<link href="css/calendar.min.css" rel="stylesheet">
<link href="css/style.typeahead.css" rel="stylesheet">
<script src="js/calendar.min.js"></script>
<script src="js/typeahead.bundle.js"></script>
<script src="js/jquery.controladorEventos.js"></script>



<!-- column 2 --> 
<div class="container col col-md-12">
  <h3 >SUB DIRECCIÓN DE LICENCIAS DE CONDUCIR</h3>  
  <input id="data-fecha" type="hidden" value="">
  <input id="data-operacion" type="hidden" value="">
  <hr>
	   <div class="row">
      <div class="col-md-12">
        
        <div id="formulario" >
          <div class="table">
          
           <div class="row" id="nuevo_usuario">
           	<div id="add_reg" class="col col-md-8" >
            <div class="panel panel-primary">
              <div class="panel-heading">
                <div class="panel-title">
                  <i class="glyphicon glyphicon-wrench pull-right"></i>
                  <h4>Perfil de Usuario</h4>
                </div>
              </div>
              <div class="panel-body">
                 <table class='table' >
                  <tr>
                    <td>
                      <label>DNI</label>
                      <div  id="the-basics" >
                        <input value="{{Persona::find(Auth::user()->persona_id)->dni}}" id="data-dni" type="text" class="typeahead form-control" placeholder="DNI" size=4 disabled>
                        <input id="data-persona_id" type="hidden" value="">
                      </div>      
                    </td>
                    <td id="apellidos">
                      <label>Apellidos</label>
                      <div class="controls">
                        <input value="{{Persona::find(Auth::user()->persona_id)->apellidos}}" id="data-apellidos" type="text" class="form-control" placeholder="Apellidos" disabled>
                      </div>
                    </td>
                    <td id="nombres" >
                      <label>Nombres</label>
                      <div class="controls">
                        <input value="{{Persona::find(Auth::user()->persona_id)->nombres}}" id="data-nombres" type="text" class="form-control" placeholder="Nombres" disabled>
                      </div>
                    </td>

                  </tr><!--end row-->
                  <tr>
                  	<td id="email" >
                      <label>Email</label>
                      <div class="controls">
                        <input id="data-email" value="{{Auth::user()->email}}" type="text" class="form-control" placeholder="Email" data-value="0" disabled>
                        
                      </div>
                    </td>
                    <td>
                      <label >Rol</label>
                      <?php
                      $type[0]="Administrador";
                      $type[1]="Digitador";
                      ?>
                      <div class="controls">
                        <input id="data-rol" value="{{$type[Auth::user()->type]}}" type="text" class="form-control" placeholder="Email" data-value="0" disabled>
                      </div>
                    </td>
                    <td>
                      <label>Trámite</label>
                      <div class="controls">
                        <input value="{{Auth::user()->tramite}}" id="data-tramite" type="text" class="form-control" placeholder="Trámite" disabled>
                      </div>
                    </td>
                  </tr>
                  <tr>
                    <td id="celular" >
                      <label>Celular</label>
                      <div class="controls">
                        <input value="{{Auth::user()->celular}}" id="data-celular" type="text" class="form-control" placeholder="Celular" disabled>
                      </div>
                    </td>
                    <td>
                      <label >Actualiza tu password</label>
                      <div class="controls">
                        <input id="data-pass" type="password" class="form-control" placeholder="password">
                      </div>
                    </td>
                    <td>
                      <label></label>
                      <div id="info_email"></div>
                    </td>
                  </tr>
                </table>
               
                <div class="controls">
                  <center>
                  <a href="home"><button id="cancelarUsuario" class="btn btn-default btn-success" type="button" value="Cancelar Registro" >Cancelar</button></a>
                  <input id="actualizarPassword" class="agregar_registro btn btn-default btn-success" type="button" value="Actualizar Datos" ></input>
                  </center>
                </div>
              </div><!--end add_reg-->
              </div><!--end panel body-->

            </div><!--end panel col -md-8-->
                        
            <div id="resultados" class="col col-md-4" >
            <div class="panel panel-primary">
              <div class="panel-heading">
                <div class="panel-title">
                  <i class="glyphicon glyphicon-wrench pull-right"></i>
                  <h4>Consejos</h4>
                </div>
              </div>
              <div class="panel-body">
              <p><b>Actializa Password:</b> Solo tiene que ingresar el nuevo password.</p>
              <p>Los cambios pueden reflejarse al cerrar sesión</p>
               
              </div>
              
              <div >
                Aqui los enlaces para retornar
                <a id="retornar" href="salir"><button>Retornar</button></a>
              </div>

            </div>
          </div>


           </div>
            


            </div>
              
              </div>

            </div>
            
          </div>
       
        </div>
        
     </div>
</div> <!--end class container-->
@else
	Error de sesión  ...!<br>
	<a href="{{ URL::route('salir') }}"><input class="btn btn-default btn-success" type="button" value="Iniciar Sesión"></a>
  
@endif

@stop