<!DOCTYPE html>
<html lang="es">

<head>	
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="destinado para la reserva de examenes">
    <meta name="keywords" content="registro de examen">
    <meta name="author" content="practicantes de DRTYCTACNA">
        
	<title>Reserva de Examenes</title>

	<!-- Mobile Specific Metas
	  ================================================== -->
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0">
	<meta name="format-detection" content="telephone=no">
	
	<!-- Bootstrap core CSS
	  ================================================== -->
	
	<!--link href="css/bootstrap.min.css" rel="stylesheet"-->
	<link href="css/bootstrap.min.css" rel="stylesheet">
	<script src="js/jquery-1.8.2.js" ></script>	
	<!---script src="js/bootstrap.min.js"></script-->
	<script src="js/bootstrap.js"></script>
	<!--script src="js/calendar.min.js"></script-->
	
	
	

</head>

<body class="home">

	<div class="body">
		<!-- Menu Principal - Botonera TOP-->
		<header class="site-header">
			@include('layouts.backend.secciones.header')
		</header>
		<!-- toolbox-->		
		
		<div class="container">
  
		  <!-- upper section -->
		  <div class="row">
			<div class="col-sm-2">
			<!-- left -->
				@include('layouts.backend.secciones.toolbox')
				<hr>
      		</div><!-- /span-3 -->
		  	<div class="col-sm-10">

		  		@yield('content')
		  	</div><!--/col-span-9-->

		  </div><!--/row-->
			<br>
		 <hr>
		<!--footer start-->
		<footer class="site-footer">
			@include('layouts.secciones.footer')
		</footer>
		
		
		<!--footer end-->
	</div>

	<!-- js placed at the end of the document so the pages load faster -->
	
	


</body>
</html>
