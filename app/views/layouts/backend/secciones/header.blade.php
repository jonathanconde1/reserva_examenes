<!-- Header -->
<div id="top-nav" class="navbar navbar-inverse navbar-static-top">
  <div class="container">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
          <span class="icon-toggle"></span>
      </button>
      <!--a class="navbar-brand" href="#">Control Panel</a-->
      <div class="navbar-brand" id="clock"></div>
    </div>
    
    <div class="navbar-collapse collapse">
      @if(Auth::check())
      <ul class="nav navbar-nav navbar-right">
        
        <li class="dropdown">
          <a class="dropdown-toggle" role="button" data-toggle="dropdown" href="#">
            <i class="glyphicon glyphicon-user"></i> {{Auth::user()->email}}  <span class="caret"></span></a>
          <ul id="g-account-menu" class="dropdown-menu" role="menu">
            <li><a href="perfil">Mi Perfil</a></li>
            <li><a href="{{ URL::route('salir') }}">Cerrar Sesión</a></li>
          </ul>
        </li>
      </ul>
      @endif
    </div>
  </div><!-- /container -->
</div>
<!-- /Header -->