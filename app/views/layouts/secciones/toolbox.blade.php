@if(Auth::check()&&Auth::user()->active=='1')
<h3>Categorias:</h3>
    
    <hr>
    <ul class="nav nav-stacked">
    @if(isset($categorias))
	@foreach($categorias as $categoria)
		<li><a id="categoria{{$categoria->id}}" href="javascript:;">{{$categoria->nombre}}</a></li>
	@endforeach
	@endif


	    
        
        
    </ul>
	<hr>
<h3>Opciones:</h3>
	
	<ul class="nav nav-stacked">
		<li><a class="cancelar" href="javascript:;">Ver Calendario</a></li>
		@if((Auth::check())&&(Auth::user()->type=='0'))
        <li><a  href="panel">Ver Panel Admin</a></li>
        @endif
        <li><a href="{{ URL::route('home') }}">Actualizar</a></li>
        <li><a class="cancelar" href="{{ URL::route('salir') }}">Cerrar Sesión</a></li>
	</ul>
@endif
  