@extends('layouts.base')
@section('content')
<div class="panel panel-default">
        <div class="panel-heading">
          <div class="panel-title">
            <i class="glyphicon glyphicon-wrench pull-right"></i>
            <h4>Panel de Ingreso</h4>
          </div>
        </div>
        <div class="panel-body">
          @if ($errors->has())
          <div class="alert alert-danger">
              @foreach ($errors->all() as $error)
                  {{ $error }}<br>        
              @endforeach
          </div>
          @endif
          <form class="form form-vertical" method="post" action="login">
            <div class="control-group">
              <label>Correo</label>
              <div class="controls">
                <input type="text" name="email" class="form-control" placeholder="Correo Electronico">
              </div>
            </div>      
            <div class="control-group">
              <label>Password</label>
              <div class="controls" align="center">
                <input type="password" name="password" class="form-control" placeholder="Password">
                
              </div>
            </div>   
            
            <div class="control-group">
              <label></label>
              <div class="controls">
                <button type="submit" class="btn btn-primary">
                  Ingresar
                </button>
                <a href="crearCuenta">Crear Cuenta</a>
              </div>
            </div>   
            
          </form>
          
          
        </div><!--/panel content-->
      </div><!--/panel-->
@stop