<?php

class RegistroController extends BaseController {
	
	public function postCantidadRegistros() { //funcion que regresa como resultado el dia y la cantidad de registros por dia
	date_default_timezone_set('America/Lima');	

    $mes = Input::get("mes");
    $mes = $mes +1; // el mes empieza en 0 = enero
    $anio = Input::get("anio");
    $categoria_id = Input::get("categoria_id");

	
	//$mes = 5; //test
	//$anio = 2015; //test
	//$categoria_id = 1; //test

	//primero consulto los dias en los que hay grupos creados
	$grupos = Group::where("mes","=",$mes)->where("anio","=",$anio)->where("categoria_id","=",$categoria_id)->get();
	foreach ($grupos as $grupo) {
		$arrayGroup_dias[] = $grupo->dia;
		$arrayGroup_ids[] = $grupo->id;
	}

	$cant = count($grupos); // cuenta la cantidad de elementos de un array

	for($i=0;$i<$cant;$i++){
		$cantidad[] = Registro::where("group_id","=",$arrayGroup_ids[$i])->where("status","=",1)->get();
		$numero= count($cantidad[$i]);
		//echo $numero[$i]."<br>";
		//resultados tiene formato   dia - cantidad de registros
		$porcentaje = ($numero * 100)/15;

		$resultados[$i] = $arrayGroup_dias[$i]."-".$numero."-".$porcentaje."-".$arrayGroup_ids[$i];
	}
	
	$resultados = json_encode($resultados);

	//echo $resultados;
	return $resultados;


	}

	public function postVerRegistro(){
		date_default_timezone_set('America/Lima');
		
		$dia = Input::get("dia");
		$mes = Input::get("mes");
	    $anio = Input::get("anio");
	    $categoria_id = Input::get("categoria_id");

	    $mes = $mes +1;
	    
		/*
		$dia = 10;
		$mes = 6; //test
		$anio = 2015; //test
		$categoria_id = 1; //test
		*/
		
		$grupos = Group::where("mes","=",$mes)->where("anio","=",$anio)->where("categoria_id","=",$categoria_id)->where("dia","=",$dia)->get();
		//el controlador dice si es 0 = no editable o 1 = editable
		
		
		if($grupos){
			foreach ($grupos as $grupo) {
				$group_id = $grupo->id;
			}
			//$group_id=15;
			if(isset($group_id)){
				$registros = Registro::where("group_id","=",$group_id)->where("status","=",1)->get();

				if(count($registros)>0){
					foreach ($registros as $registro) {
					$persona = Persona::find($registro->persona_id);
					$categoriaDetalle = CategoryDetail::find($registro->cat_detalle_id);
					$tipo = Tipo::find($registro->tipo_id);
					$user = User::find($registro->user_id);
					$editable = 0;


					$userActual = Auth::user()->id;
					$userReg = $user->id;
						
						if($userReg == $userActual){
							$editable = 1;
						}

					$arrayRegistro[] = $persona->dni."-".$persona->apellidos.", ".$persona->nombres."-".$categoriaDetalle->name."-".$tipo->name."-".$user->tramite."-".$registro->a."-".$registro->d."-".$registro->nsp."-".$registro->observacion."-".$editable."-".$registro->id;

					}

				$resultados = json_encode($arrayRegistro);
				
				return $resultados;

				//echo $arrayRegistro[0];
				
					
				}else{
					return "error";	
				}
				
				
			}else{
				return "error";
			}
			
			
		}else{
			return "error";
		}
		
		



	}

	public function postIngresarRegistro(){
		date_default_timezone_set('America/Lima');
		/*
			Detectar si existe DNI se cancela si se envia el id persona OK
		*/
		/*
		//echo "ruta creada XD"; 
		 //parametros para testear el controlador		
		$persona_id = "";
		$persona_dni = "42500932";
		$persona_apellidos="Quispe Quispe";
		$persona_nombres="Karla";
		$dia = 1;
		$mes = 6; //test
		$anio = 2015; //test
		$categoria_id = 1; //test solo para efectos de busquera de grupo
		$cat_detalle_id = 1;
		
		$tipo_id = 3;
		
		//$user_id = 1;
		//$code = "xEW18uDO";	
		//$code = Input::get("code");	
		$observacion = "3ra vez ";
		*/
	
		$persona_id = Input::get("persona_id");
		$persona_dni = Input::get("persona_dni");
		$persona_apellidos= Input::get("persona_apellidos");
		$persona_nombres=Input::get("persona_nombres");
		$dia = Input::get("dia");
		$mes = Input::get("mes");
		$mes = $mes +1;
		$anio = Input::get("anio");
		$categoria_id = Input::get("categoria_id");
		$cat_detalle_id = Input::get("cat_detalle_id");
		$observacion = Input::get("observacion");

		$tipo_id = Input::get("tipo_id");
		$code = Input::get("code");	
		$user_id = Auth::user()->id;
	
		//primer paso: conseguir el id del grupo al que pertenece
		$grupos = Group::where("mes","=",$mes)->where("anio","=",$anio)->where("categoria_id","=",$categoria_id)->where("dia","=",$dia)->get();
		foreach ($grupos as $grupo) {
				$group_id = $grupo->id;
		}
		if(isset($group_id)){
			//sigo con el proceso
			//echo "grupo encontrado";
			//echo " -- id del grupo == ".$group_id;
		}else{ //genero el grupo
			echo "grupo no encontrado";
			$grupo = new Group();
			$grupo->dia = $dia;
			$grupo->mes = $mes;
			$grupo->anio = $anio;
			$grupo->categoria_id = $categoria_id;
			$grupo->active = 1;
			$grupo->save();
			//echo "<br>grupo guardado";

			$group_id = $grupo->id;
			//echo "id del grupo == ".$group_id;
		}
		// segundo : como ya tenemos identificado el grupo
		// tenemos que identificar a la persona
		if($persona_id==""){
			//echo "id de persona no encontrada";
			// entonces a buscar el id de la persona por el dni
			$ids = Persona::where("dni","=",$persona_dni)->get();

			foreach ($ids as $id) {
				$id_persona = $id->id;
			}
			
			if(!(isset($id_persona))){

				//entonces nuevo registro de persona
				
				$persona = new Persona();
				$persona->dni = $persona_dni;
				$persona->nombres = $persona_nombres;
				$persona->apellidos = $persona_apellidos;
				$persona->save();

				$persona_id = $persona->id;

				//echo "<br> persona guardad con id ==".$persona_id;
			}else{
				$persona_id = $id_persona;
				//echo "<br>persona encontrada por dni id = ".$persona_id;
			}
			



		}else{
			//echo "id de persona si encontrada";
		}
		//consiguiendo key del usuario actual

		/* // parametros a insertar
		$persona_id = "";
		$persona_dni = "42500932";
		$persona_apellidos="Quispe Condori";
		$persona_nombres="Yessenia";
		$dia = 11;
		$mes = 5; //test
		$anio = 2015; //test
		$categoria_id = 1; //test solo para efectos de busquera de grupo
		
		$tipo_id = 3;
		$user_id = 1;
		$observacion = "3ra vez ";
		*/
		//si no se especifica persona_id, se busca en este controlador
		// $group_id //el  cual se busca mediante este controlador
		// $key_id //el  cual se busca mediante este controlador
		//una vez solucionado el problema con el grupo y los dni solo
		// nos queda guardar el registro

		
		$llaves = Key::where("code","=",$code)->get();

		foreach ($llaves as $key) {
			//echo "llave".$key->code."id_user = ".$key->user_id;
		}

		if(isset($key->code)){
			//echo "llave asignado del otro usuario";
			$key_id = $key->id;
		}else{
			//echo "no existe la llave<br>";
			$keys = Key::where("user_id","=",$user_id)->get();

			foreach ($keys as $key) {
				
				$key_id = $key->id;
			}
			if(isset($key_id)){
				//echo "llave asignado del mismo usuario";
			}else{
				//echo "no hay llave de usuario";
			}
		}

		

		

		$registro = new Registro();
		$registro->status = 1;
		$registro->tipo_id = $tipo_id;
		$registro->user_id = $user_id; //para efectos de tramite
		$registro->persona_id= $persona_id;
		$registro->group_id = $group_id;
		$registro->key_id = $key_id; // necesito logica para id key
		$registro->cat_detalle_id = $cat_detalle_id;
		$registro->observacion = $observacion;
		$registro->save();

		//echo "guardo con exito";

		//ahora solo me falta guardar la operacion realizada
		$regOperacion = new Regoperacion();
		$regOperacion->operacion_id = 1; //crear registro
		$regOperacion->user_id = Auth::user()->id;
		$regOperacion->registro_id = $registro->id;
		$regOperacion->save();

		//eliminando llave temporal
		if($code!=""){
			$llave =  Key::find($key_id);
			$llave->status = 0;
			$llave->save();	
		}
		



		

	}

	public function postMoverRegistro(){
		date_default_timezone_set('America/Lima');
		/*
		$registro_id = "60";
		$dia = 23;
		$mes = 5;
		$anio = 2015;
		$categoria_id = 1;
*/
		
		$registro_id = Input::get("registro_id");
		$dia = Input::get("dia");
		$mes = Input::get("mes");
		$mes = $mes +1;
		$anio = Input::get("anio");
		$categoria_id = Input::get("categoria_id");

		$grupos = Group::where("dia","=",$dia)->where("mes","=",$mes)->where("anio","=",$anio)->where("categoria_id","=",$categoria_id)->where("active","=","1")->get();

		foreach ($grupos as $grupo) {
			$grupo_id = $grupo->id;	
		}

		if(isset($grupo_id)){
			echo "existe el grupo = ".$grupo_id;
			//$grupo_id = $grupo->id;
		}else{
			echo "no exite el grupo y tenemos que crearlo";
			$grupo_nuevo = new Group();
			$grupo_nuevo->dia = $dia;
			$grupo_nuevo->mes = $mes;
			$grupo_nuevo->anio = $anio;
			$grupo_nuevo->active = 1;
			$grupo_nuevo->categoria_id = $categoria_id;
			$grupo_nuevo->save();

			$grupo_id = $grupo_nuevo->id;
		}

		$registro = Registro::find($registro_id);
		$registro->group_id = $grupo_id;
		$registro->save();
	
		//falta implementar guardar operacion
		$regOperacion = new Regoperacion();
		$regOperacion->operacion_id = 4; //mover registro
		$regOperacion->user_id = Auth::user()->id;
		$regOperacion->registro_id = $registro->id;
		$regOperacion->save();

		return $grupo_id;	
	
	}

	public function postVerificarGrupo(){
		date_default_timezone_set('America/Lima');
		
		//$registro_id = "60";
		/*$dia = 23;
		$mes = 5;
		$anio = 2015;
		$categoria_id = 1;*/

		

		$dia = Input::get("dia");
		$mes = Input::get("mes");
		$mes = $mes +1;
		$anio = Input::get("anio");
		$categoria_id = Input::get("categoria_id");

		$grupos = Group::where("dia","=",$dia)->where("mes","=",$mes)->where("anio","=",$anio)->where("categoria_id","=",$categoria_id)->where("active","=","1")->get();

		if($grupos){
			foreach ($grupos as $grupo) {
			$grupo_id = $grupo->id;	
		}	
		}
		

		if(isset($grupo_id)){
			//echo "existe el grupo = ".$grupo_id;
			//$grupo_id = $grupo->id;
		}else{
			//echo "no exite el grupo y tenemos que crearlo";
			
			$grupo_nuevo = new Group();
			$grupo_nuevo->dia = $dia;
			$grupo_nuevo->mes = $mes;
			$grupo_nuevo->anio = $anio;
			$grupo_nuevo->active = 1;
			$grupo_nuevo->categoria_id = $categoria_id;
			$grupo_nuevo->save();

			$grupo_id = $grupo_nuevo->id;
			
		}

		

		return $grupo_id;	
		
	}

	public function postEliminarRegistro(){
		date_default_timezone_set('America/Lima');
		/*
		$registro_id = "40";
		$dia = 4;
		$mes = 4;
		$anio = 2015;
		$categoria_id = 1;
*/
		
		$registro_id = Input::get("registro_id");
		$dia = Input::get("dia");
		$mes = Input::get("mes");
		$mes = $mes +1;
		$anio = Input::get("anio");
		$categoria_id = Input::get("categoria_id");

		$grupos = Group::where("dia","=",$dia)->where("mes","=",$mes)->where("categoria_id","=",$categoria_id)->where("active","=","1")->get();

		foreach ($grupos as $grupo) {
			
		}

		if(isset($grupo->id)){
			//echo "existe el grupo = ".$grupo->id;
			$grupo_id = $grupo->id;
		}else{
			//echo "no exite el grupo y tenemos que crearlo";
			$grupo_nuevo = new Group();
			$grupo_nuevo->dia = $dia;
			$grupo_nuevo->mes = $mes;
			$grupo_nuevo->anio = $anio;
			$grupo_nuevo->active = 1;
			$grupo_nuevo->categoria_id = $categoria_id;
			$grupo_nuevo->save();

			$grupo_id = $grupo_nuevo->id;
		}

		$registro = Registro::find($registro_id);
		$registro->status = 0;
		$registro->save();
	
		//falta implementar guardar operacion
		$regOperacion = new Regoperacion();
		$regOperacion->operacion_id = 3;
		$regOperacion->user_id = Auth::user()->id;
		$regOperacion->registro_id = $registro->id;
		$regOperacion->save();

		return "id_grupo = ".$grupo_id;	
	
	}




}