<?php

class ReporteController extends BaseController {
	
	public function reporteExcel(){

       $array_dia = ["Domingo","Lunes","Martes","Miercoles","Jueves","Viernes","Sabado"];
       $array_mes = ["Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"];
       $array_tipo = ["","","","ORIGINAL","RECATEG"];
       $array_categoria = ["","AI","AII","AIII"];
       $array_categoria_hora = ["","9:00 AM","11:00 AM","2:00 PM"];

       $dia = Input::get("dia");
       $dia_id = Input::get("dia_id");
       $mes = Input::get("mes");
       $anio = Input::get("anio");
       $categoria_id = Input::get("categoria_id");


	error_reporting(E_ALL);
    ini_set('display_errors', TRUE);
    ini_set('display_startup_errors', TRUE);
    date_default_timezone_set('Europe/London');
    if (PHP_SAPI == 'cli')
      die('This example should only be run from a Web Browser');

     
    

		// Crea un nuevo objeto PHPExcel
		$objPHPExcel = new PHPExcel();

		// Se asignan las propiedades del libro
		$objPHPExcel->getProperties()->setCreator("Codedrinks") //Autor
							 ->setLastModifiedBy("Antonny") //Ultimo usuario que lo modificó
							 ->setTitle("SUB DIRECCION DE LICENCIAS DE CONDUCIR")
							 ->setSubject("sub direccion de licencias de conducir")
							 ->setDescription("Reporte de examenes de manejo")
							 ->setKeywords("Reporte de examenes de manejo")
							 ->setCategory("Reporte excel");

		$tituloReporte = "SUB DIRECCION DE LICENCIAS DE CONDUCIR";
		$titulosColumnas = array('Nº', 'DNI', 'APELLIDOS Y NOMBRE', 'CAT', 'TIPO', 'TRAMITE', 'A', 'D', 'NSP', 'OBSERV.');

		$objPHPExcel->setActiveSheetIndex(0)
        		    ->mergeCells('A1:J1')
                    ->mergeCells('D5:E5')
                    ->mergeCells('C2:F2');

        // Se agregan los titulos del reporte
        $objPHPExcel->setActiveSheetIndex(0)
				->setCellValue('A1', $tituloReporte)
                ->setCellValue('B2', 'EVALUADOR:')
                ->setCellValue('C4', 'RELACION DE EXAMEN DE MANEJO CATEGORIA '.$array_categoria[$categoria_id])
                ->setCellValue('B5', 'FECHA:')
                ->setCellValue('C5', ''.$array_dia[$dia_id].' '.$dia.' de '.$array_mes[$mes].' del '.$anio)
                ->setCellValue('D5', ''.$array_categoria_hora[$categoria_id])
                ->setCellValue('A6',  $titulosColumnas[0])
        		    ->setCellValue('B6',  $titulosColumnas[1])
                ->setCellValue('C6',  $titulosColumnas[2])
                ->setCellValue('D6',  $titulosColumnas[3])
                ->setCellValue('E6',  $titulosColumnas[4])
                ->setCellValue('F6',  $titulosColumnas[5])
                ->setCellValue('G6',  $titulosColumnas[6])
                ->setCellValue('H6',  $titulosColumnas[7])
                ->setCellValue('I6',  $titulosColumnas[8])
            	->setCellValue('J6',  $titulosColumnas[9]);


    $i = 7;
    $mes = $mes +1;

    $grupos = Group::where("mes","=",$mes)->where("anio","=",$anio)->where("categoria_id","=",$categoria_id)->where("dia","=",$dia)->get();
    //el controlador dice si es 0 = no editable o 1 = editable
    
    
    if($grupos){
      foreach ($grupos as $grupo) {
        $group_id = $grupo->id;
      }
      if(isset($group_id)){
        $registros = Registro::where("group_id","=",$group_id)->where("status","=",1)->get();


        foreach ($registros as $registro) {
          $persona = Persona::find($registro->persona_id);
          $categoriaDetalle = CategoryDetail::find($registro->cat_detalle_id);
          $tipo = Tipo::find($registro->tipo_id);
          $user = User::find($registro->user_id);
          $editable = 0;


          $userActual = Auth::user()->id;
          $userReg = $user->id;
          if($userReg == $userActual){
            $editable = 1;
          }

          

          $arrayRegistro[] = $persona->dni."-".$persona->apellidos.", ".$persona->nombres."-".$categoriaDetalle->name."-".$tipo->name."-".$user->tramite."-".$registro->a."-".$registro->d."-".$registro->nsp."-".$registro->observacion."-".$editable."-".$registro->id;
          
          $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('A'.$i, $i-6)
                ->setCellValue('B'.$i, $persona->dni)
                ->setCellValue('C'.$i, $persona->apellidos.", ".$persona->nombres)
                ->setCellValue('D'.$i, $categoriaDetalle->name)
                ->setCellValue('E'.$i, $array_tipo[$tipo->id])
                ->setCellValue('F'.$i, $user->tramite)
                ->setCellValue('G'.$i, $registro->a)
                ->setCellValue('H'.$i, $registro->d)
                ->setCellValue('I'.$i, $registro->nsp)
                ->setCellValue('J'.$i, $registro->observacion);
          $i++;

        }

        $resultados = json_encode($arrayRegistro);
      }else{
        //return "error";
      }
      
      
    }else{
      return "grupo no encontrado";
    }
    


        if($i<22){
          for ($j=$i; $j <22 ; $j++) { 
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'.$j, $j-6);
          }
          $i = 22;
        }

        
        $objPHPExcel->setActiveSheetIndex(0)
                    ->mergeCells('F'.($i+3).':J'.($i+3));

        $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('B'.$i, 'LEYENDA:')
                ->setCellValue('C'.$i, 'APROBO')
                ->setCellValue('C'.($i+1), 'DESAPROBO')
                ->setCellValue('C'.($i+2), 'NO SE PRESENTO')
                ->setCellValue('F'.($i+3), 'FIRMA Y SELLO DEL EVALUADOR');



        $estiloTituloReporte = array(
                'font'      => array('bold' => true),
                'alignment' =>  array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                                      'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                                      'wrap'       => TRUE)
        );

        $estiloCampoEvaluador = array(
                'borders' => array(//'top' => array('style' => PHPExcel_Style_Border::BORDER_THIN ,'color' => array('rgb' => '000000')),
                                   'bottom' => array('style' => PHPExcel_Style_Border::BORDER_THIN ,'color' => array('rgb' => '000000')))
                                   //'right'  => array('style' => PHPExcel_Style_Border::BORDER_THIN ,'color' => array('rgb' => '000000')),
                                   /*'left'     => array('style' => PHPExcel_Style_Border::BORDER_THIN ,'color' => array('rgb' => '000000')))*/
         );
        $estiloCampoCentrado = array(
                'alignment' =>  array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                                      'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                                      'wrap'       => TRUE)
        );
        $estiloCampoIzquierda = array(
                'alignment' =>  array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
                                      'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                                      'wrap'       => TRUE)
        );


        $estiloInformacion = new PHPExcel_Style();
        $estiloInformacion->applyFromArray(array(
                /*'font' => array('name'  => 'Arial',
                                'color'     => array('rgb' => '000000')),*/
                /*'fill'  => array('type'  => PHPExcel_Style_Fill::FILL_SOLID,
                             'color' => array('argb' => 'FFd9b7f4')),*/
                'borders' => array('top' => array('style' => PHPExcel_Style_Border::BORDER_THIN ,'color' => array('rgb' => '000000')),
                                   'bottom' => array('style' => PHPExcel_Style_Border::BORDER_THIN ,'color' => array('rgb' => '000000')),
                                   'right'  => array('style' => PHPExcel_Style_Border::BORDER_THIN ,'color' => array('rgb' => '000000')),
                                   'left'     => array('style' => PHPExcel_Style_Border::BORDER_THIN ,'color' => array('rgb' => '000000')))
        ));

        $estiloColumnas = new PHPExcel_Style();
        $estiloColumnas->applyFromArray(array(
                'font' => array('bold'  => true,  'color' => array('rgb' => '000000')),
                /*'fill'  => array('type'  => PHPExcel_Style_Fill::FILL_SOLID,
                             'color' => array('argb' => 'FFd9b7f4')),*/
                'borders' => array('top' => array('style' => PHPExcel_Style_Border::BORDER_THIN ,'color' => array('rgb' => '000000')),
                                   'bottom' => array('style' => PHPExcel_Style_Border::BORDER_THIN ,'color' => array('rgb' => '000000')),
                                   'right'  => array('style' => PHPExcel_Style_Border::BORDER_THIN ,'color' => array('rgb' => '000000')),
                                   'left'     => array('style' => PHPExcel_Style_Border::BORDER_THIN ,'color' => array('rgb' => '000000'))),
                'alignment' =>  array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                                      'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                                      'wrap'       => TRUE)
        ));

        $objPHPExcel->getActiveSheet()->getStyle('A1:J1')->applyFromArray($estiloTituloReporte);
        $objPHPExcel->getActiveSheet()->getStyle('C2:F2')->applyFromArray($estiloCampoEvaluador); 
        $objPHPExcel->getActiveSheet()->getStyle('B5')->applyFromArray($estiloTituloReporte); 
        $objPHPExcel->getActiveSheet()->getStyle('D5')->applyFromArray($estiloCampoCentrado);
        $objPHPExcel->getActiveSheet()->getStyle('B'.$i)->applyFromArray($estiloCampoCentrado);
        

        $objPHPExcel->getActiveSheet()->setSharedStyle($estiloColumnas, "A6:J6");
        $objPHPExcel->getActiveSheet()->setSharedStyle($estiloInformacion, "A7:J".($i-1));

        $objPHPExcel->getActiveSheet()->getStyle('A7:J'.($i-1))->applyFromArray($estiloCampoCentrado);
        $objPHPExcel->getActiveSheet()->getStyle('C7:C'.($i-1))->applyFromArray($estiloCampoIzquierda);

        $objPHPExcel->getActiveSheet()->getStyle('A7:A'.($i-1))->getNumberFormat()->setFormatCode('00');
        $objPHPExcel->getActiveSheet()->getStyle('B7:B'.($i-1))->getNumberFormat()->setFormatCode('00000000');

        $objPHPExcel->setActiveSheetIndex(0)->getColumnDimension('A')->setWidth(5);
        $objPHPExcel->setActiveSheetIndex(0)->getColumnDimension('B')->setWidth(12);
        $objPHPExcel->setActiveSheetIndex(0)->getColumnDimension('C')->setWidth(50);
        $objPHPExcel->setActiveSheetIndex(0)->getColumnDimension('D')->setWidth(8);
        $objPHPExcel->setActiveSheetIndex(0)->getColumnDimension('E')->setWidth(12);
        $objPHPExcel->setActiveSheetIndex(0)->getColumnDimension('F')->setWidth(10);
        $objPHPExcel->setActiveSheetIndex(0)->getColumnDimension('G')->setWidth(6);
        $objPHPExcel->setActiveSheetIndex(0)->getColumnDimension('H')->setWidth(6);
        $objPHPExcel->setActiveSheetIndex(0)->getColumnDimension('I')->setWidth(6);
        $objPHPExcel->setActiveSheetIndex(0)->getColumnDimension('J')->setWidth(15);


        $objPHPExcel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
        $objPHPExcel->getActiveSheet()->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);

		// Renombrar Hoja
        $objPHPExcel->getActiveSheet()->setTitle('CATEGORIA - '.$array_categoria[$categoria_id]);

		// Establecer la hoja activa, para que cuando se abra el documento se muestre primero.
		$objPHPExcel->setActiveSheetIndex(0);

		// Se modifican los encabezados del HTTP para indicar que se envia un archivo de Excel.
		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="Examen Manejo - '.$array_dia[$dia_id].' '.$dia.' de '.$array_mes[$mes].' del '.$anio.' - Categoria '.$array_categoria[$categoria_id].'.xlsx"');
		header('Cache-Control: max-age=0');
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
		$objWriter->save('php://output');
		exit;

	}



	
}