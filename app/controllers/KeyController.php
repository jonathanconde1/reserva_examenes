<?php

class KeyController extends BaseController {
	public function postValidar(){
		//echo "exito ruta creada<br>";
		//recibe la llave y verifica retorna true o false
		//$code = "Jbkk9h2A"; //test

		$code = Input::get("code");

		$keys = Key::where("code","=",$code)->where("status","=",1)->where("type","=",1)->get();

		foreach ($keys as $key) {
			//echo "llave".$key->code."id_user = ".$key->user_id;
		}

		if(isset($key->code)){
			$user = User::find($key->user_id);
			$persona = Persona::find($user->id);

			//echo "existe la llave<br>".$persona->nombres;
			return $persona->nombres;
		}else{
			//echo "no existe la llave<br>";
			return "error";
		}


		
	}
	public function getLlave(){
		$user_id = Auth::user()->id;

		$keys = Key::where("user_id","=",$user_id)->where("type","=",1)->where("status","=",1)->get();
		//echo "controlador creado";

		//echo var_dump($keys);
		
		if (count($keys)<20){
			$cant = 20 - count($keys);
			//echo "Se generan llaves faltantes";
			for($i = 0; $i < $cant; $i++){
				$llave = new key();
				$llave->code = str_random(8);
				$llave->type = 1; //0 = privando , 1= publico
				$llave->status = 1; //0 = inactivo, 1 = activo
				$llave->user_id = $user_id;
				$llave->save();

			}
		}else{
			//echo "llaves completas";
		}

		$keys = Key::where("user_id","=",$user_id)->where("type","=",1)->where("status","=",1)->get();

		foreach ($keys as $key) {
		//	echo $key->code."<br>";
		}

		return View::make('admin.llave')->with(array('keys'=>$keys));

		//echo count($keys);

	}
}