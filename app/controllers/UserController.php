<?php

class UserController extends BaseController {
	
	public function postDni() {
		date_default_timezone_set('America/Lima');
		$dni = Input::get("dni");
		//$dni = 4250; //test
		
		$persona = Persona::where("dni","=",$dni)->get();
		
		if($persona){
			foreach ($persona as $datos) {
				$nombres = $datos->nombres;
				$apellidos = $datos->apellidos;
				$id = $datos->id;

			}
			
			$datos = $id.", ".$apellidos.", ".$nombres;
			return $datos;
		}else{
			//no funciona x q jquery me arroja fail
			return "error";		
		}
	
		


	}

	public function getCrearUsuario(){

		return View::make('crearUsuario');
	}

	public function getPanel(){
		//echo "ruta creada";
		return View::make('admin.panel');
	}

	public function postVerUsuario(){
		date_default_timezone_set('America/Lima');
		$rol[0]="Admin.";
		$rol[1]="Digit.";

		$active[1]="Activo";
		$active[2]="Por Activar";
		$active[3]="Por Activar";

		if((Auth::check())&&(Auth::user()->type=='0')){
			$users = User::where('active','>','0')->get();

			//echo var_dump($users);
			foreach ($users as $user) {
				//echo  Persona::find($user->persona_id)->dni."-".Persona::find($user->persona_id)->apellidos.", ".Persona::find($user->persona_id)->nombres."-".$user->email."-".$rol[$user->type]."-".$user->tramite;
				if($user->id!=Auth::user()->id){
					$arrayUsuario[] =  Persona::find($user->persona_id)->dni."-".Persona::find($user->persona_id)->apellidos.", ".Persona::find($user->persona_id)->nombres."-".$user->email."-".$rol[$user->type]."-".$user->tramite."-".$active[$user->active]."-".$user->id."<br>";		
				}
				
			}
			
			
			$resultados = json_encode($arrayUsuario);
			return $resultados;
		}
	}

	public function postEmail(){
		date_default_timezone_set('America/Lima');

		//echo "busca el email de la persona";
		//$email = "admin@admin.com"; test;
		$email = Input::get("email");
		$user="0";

		$users = User::where("email","=",$email)->get();
		foreach ($users as $user) {
			$user = "2";
		}

		if($user){
			//echo "existe";
			return 1;
			
		}else{
			//echo "no existe";
			return 0;
		}
		
	}
	
	public function getCrearCuenta(){
		//echo "busca el email de la persona";
		return View::make('crearCuenta');
		
	}

	

	public function postIngresarUsuario(){
		date_default_timezone_set('America/Lima');
		//echo "controlar creado";
		
		$data_dni =Input::get("data_dni");
     	$data_persona_id = Input::get("data_persona_id");
      	$data_apellidos = Input::get("data_apellidos");
      	$data_nombres = Input::get("data_nombres");
      	$data_email = Input::get("data_email");
      	
      	$data_rol = Input::get("data_rol");
      	$data_tramite = Input::get("data_tramite");
      	$data_celular = Input::get("data_celular");
      	$data_pass = Input::get("data_pass");
		

      	if($data_persona_id==""){ // crear reg persona
      		$persona = new Persona();
      		$persona->dni = $data_dni;
      		$persona->nombres = $data_nombres;
      		$persona->apellidos = $data_apellidos;
      		$persona->save();

      		$data_persona_id = $persona->id;	
      	}

      	$usuario = new User();
      	$usuario->email = $data_email;
      	$usuario->password = Hash::make($data_pass);
      	$usuario->tramite = $data_tramite;
      	$usuario->celular = $data_celular;
      	$usuario->photo = "image1.jpg";
      	$usuario->active = 2; // 1= activo, 0=inactivo, 2 = por confirmar
      	$usuario->type= $data_rol;
      	$usuario->persona_id= $data_persona_id;
      	$usuario->save();

      	$key = new Key();
      	$key->code = str_random(8);
      	$key->type = 0;
      	$key->status = 1;
      	$key->user_id = $usuario->id;
      	$key->save();


	}

	public function getActivarUsuario($id){
		date_default_timezone_set('America/Lima');
		$user = User::find($id);
		$user->active = "1";
		$user->save();

		$registro = new Adminoperacion();
		$registro->admin_id = Auth::user()->id;
		$registro->user_id = $user->id;
		$registro->operacion_id = "5";
		$registro->save();
				
		return Redirect::to("panel");
	}
	public function getDesactivarUsuario($id){
		date_default_timezone_set('America/Lima');
		$user = User::find($id);
		$user->active = "2";
		$user->save();
		
		$registro = new Adminoperacion();
		$registro->admin_id = Auth::user()->id;
		$registro->user_id = $user->id;
		$registro->operacion_id = "6";
		$registro->save();
		
		return Redirect::to("panel");
	}
	public function getEliminarUsuario($id){
		date_default_timezone_set('America/Lima');
		$user = User::find($id);
		$user->active = "0";
		$user->save();
		
		$registro = new Adminoperacion();
		$registro->admin_id = Auth::user()->id;
		$registro->user_id = $user->id;
		$registro->operacion_id = "3";
		$registro->save();
		
		return Redirect::to("panel");
	}

	public function getPerfil(){
		return View::make('perfil');	
	}


}