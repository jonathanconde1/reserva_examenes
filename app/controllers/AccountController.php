<?php

class AccountController extends BaseController {

	public function postSignIn(){
		// verificamos si el usuario ha escrito los campos indicados
		$validator = Validator::make(Input::all(),
			array(
				'email' => 'required|email',
				'password' => 'required'
			)
		);

		// Validamos los datos ingresados al formulario
		if($validator->fails()) {
			
			return Redirect::route('login')
				->withErrors($validator)
				->withInput();
				
		} else {
			//echo "exito";
			
			$remember = (Input::has('remember')) ? true : false;
			
			// Autentica data validada recibida del formulario
			$auth = Auth::attempt(array(
				'email' => Input::get('email'),
				'password' => Input::get('password')
				
			), $remember);
			
			//si fue exitosa la autenticacion
			if($auth){
				//Redireccionar a la pagina intented
				if(Auth::user()->active==1){
					switch (Auth::user()->type) {
					case '0': //administrador (administra usuarios)
						return Redirect::to('panel');		
						break;
					case '1': //Digitador (administra usuarios)
						//echo "digitador";
						return Redirect::to('home');		
						break;
					case '2': //Digitador (administra usuarios)
						//echo "digitador";
						return "no se encuentra su rol ...";
						break;
					case '3': //Digitador (administra usuarios)
						//echo "digitador";
						return "no se encuentra su rol ...";
						break;
					
					default:
						return "no se encuentra su rol ...";
						break;
				}

				}else{
					Auth::logout();
					return Redirect::route('login')
					->with('global','El usuario esta en espera de confirmación por parte del Administrador');
				}


				
				
				//return Redirect::to('home');
				//echo "exito en auth";
			} else {
				
				//echo "falla en auth";
				return Redirect::route('login')
				->with('global','Email y/o contraseña incorrectas, o quizá tu cuenta no está activada');
			
					
			}
			
		}
		
		//return Redirect::route('login')->with('global','Hubo un problema para autenticarte');
	}

	public function getSignOut() {
		Auth::logout();
		//echo "exito";
		return View::make('login');	
		//return Redirect::to('/');
		//return Redirect::route('/');
	}


}