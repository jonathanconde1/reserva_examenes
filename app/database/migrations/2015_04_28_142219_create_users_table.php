<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('users', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('email');
			$table->string('password');
			$table->string('tramite');
			$table->string('celular')->nullable();
			$table->string('photo');
			$table->integer('active'); // 0= inactivo, 1 activo, 2 por activar
			$table->integer('type'); // nivel de acceso 0, 1 , 2 , 3 
			$table->integer('persona_id')->unsigned();
			$table->foreign('persona_id')->references('id')->on('personas');

			$table->rememberToken()->nullable();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('users');
	}

}
