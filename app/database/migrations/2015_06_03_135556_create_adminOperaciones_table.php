<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdminOperacionesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('adminOperaciones', function(Blueprint $table)
		{
			$table->increments('id');

			$table->integer('admin_id')->unsigned(); //administrador
			$table->foreign('admin_id')->references('id')->on('users');

			$table->integer('user_id')->unsigned(); //usuario
			$table->foreign('user_id')->references('id')->on('users');

			$table->integer('operacion_id')->unsigned(); //operacion realizada
			$table->foreign('operacion_id')->references('id')->on('operaciones');
			
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('adminOperaciones');
	}

}
