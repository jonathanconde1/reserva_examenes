<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRegOperacionesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('regOperaciones', function(Blueprint $table)
		{
			$table->increments('id');

			$table->integer('operacion_id')->unsigned(); //tramite
			$table->foreign('operacion_id')->references('id')->on('operaciones');

			$table->integer('user_id')->unsigned(); //tramite
			$table->foreign('user_id')->references('id')->on('users');

			$table->integer('registro_id')->unsigned(); //tramite
			$table->foreign('registro_id')->references('id')->on('registros');
			
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('regOperaciones');
	}

}
