<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRegTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('registros', function(Blueprint $table)
		{
			$table->increments('id');

			$table->integer('status'); //0 eliminado, 1 Activo
			$table->string('a'); 
			$table->string('d'); 
			$table->string('nsp'); 
			$table->string('observacion'); 
			
			$table->integer('tipo_id')->unsigned(); //tramite
			$table->foreign('tipo_id')->references('id')->on('tipos');			

			$table->integer('user_id')->unsigned(); //tramite
			$table->foreign('user_id')->references('id')->on('users');

			$table->integer('persona_id')->unsigned(); //persona
			$table->foreign('persona_id')->references('id')->on('personas');

			$table->integer('group_id')->unsigned(); //grupo al que corresponde
			$table->foreign('group_id')->references('id')->on('groups');

			$table->integer('key_id')->unsigned(); //permiso otorgado
			$table->foreign('key_id')->references('id')->on('keys');

			$table->integer('cat_detalle_id')->unsigned(); //permiso otorgado
			$table->foreign('cat_detalle_id')->references('id')->on('categorias_detalles');

			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('registros');
	}

}
