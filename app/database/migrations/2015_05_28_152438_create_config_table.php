<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConfigTable extends Migration {

	
	public function up()
	{
		
		Schema::create('configuraciones', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('status'); //0 inactivo, 1 Activo
			$table->string('name');
			$table->integer('user_id');
			$table->integer('max');
			
			$table->timestamps();
		
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('configuraciones');
	}

}
