<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('users', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('email');
			$table->string('password');
			$table->string('photo');
			$table->string('active'); // para dar de baja a algunos usuarios
			$table->string('type'); // nivel de acceso 0, 1 , 2 , 3 
			$table->integer('id_persona')->unsigned();
			
			$table->foreign('id_persona')->references('id')
			->on('personas')->onDelete('CASCADE')
			->onUpdate('CASCADE');

			$table->rememberToken()->nullable();
			$table->timestamps();
		});
    

	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('users');
	}

}
