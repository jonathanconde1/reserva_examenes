<?php

/**
 * Agregamos un usuario nuevo a la base de datos.
 */
class UserTableSeeder extends Seeder {
    public function run(){
		DB::table('users')->delete();
        User::create(array(
			'email' => 'admin@admin.com',
	        'password' => Hash::make('admin'), // Hash::make() nos va generar una cadena con nuestra contraseña encriptada
	        'photo' => 'image1.jpg',
            'tramite' => 'A',
            'active' => 1,
            'type' => 0,  
            'persona_id' => 1,  
        ));
        User::create(array(
            'email' => 'otro@otro.com',
            'password' => Hash::make('otro'), // Hash::make() nos va generar una cadena con nuestra contraseña encriptada
            'photo' => 'image2.jpg',
            'tramite' => 'B',
            'active' => 1,
            'type' => 1,  
            'persona_id' => 2,  
        ));
        
    }
}


