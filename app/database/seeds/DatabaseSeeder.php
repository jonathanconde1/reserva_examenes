<?php

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Eloquent::unguard();

		$this->call('PersonaTableSeeder');
		$this->call('UserTableSeeder');
		$this->call('KeyTableSeeder');
		$this->call('CategoryTableSeeder');
		$this->call('DetailCategoryTableSeeder');
		$this->call('TipoTableSeeder');
		$this->call('TipoTableSeeder');
		$this->call('OperacionTableSeeder');
		$this->call('RegistroTableSeeder');
		$this->call('ConfiguracionTableSeeder');
		
	}

}
