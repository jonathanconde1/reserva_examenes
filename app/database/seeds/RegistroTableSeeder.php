<?php
/**
 * Agregamos un usuario nuevo a la base de datos.
 */
class RegistroTableSeeder extends Seeder {
    public function run(){
		DB::table('groups')->delete(); // limpiamos el contenido de la tabla
		//creamos un grupo
		Group::create(array(
			'dia' => '14',
			'dia_n' => '4', // 4 debe ser jueves
			'mes' => '5',
			'anio' => '2015',
			'active' => '1',
			'categoria_id' => '1',
	        
        ));
        Group::create(array(
			'dia' => '15',
			'dia_n' => '5', // 4 debe ser jueves
			'mes' => '5',
			'anio' => '2015',
			'active' => '1',
			'categoria_id' => '1',
	        
        ));
        Group::create(array(
			'dia' => '18',
			'dia_n' => '1', // 4 debe ser jueves
			'mes' => '5',
			'anio' => '2015',
			'active' => '1',
			'categoria_id' => '1',
	        
        ));
        //ahora procedemos a crear el registro
        DB::table('registros')->delete(); // limpiamos el contenido de la tabla
        Registro::create(array(
			'status' => '1',
			'tipo_id' => '3',  // se genero por defecto 3 y 4
			'user_id' => '1',
			'persona_id' => '1',
			'group_id' => '1',
			'key_id' => '2',
			'cat_detalle_id' => '1',
	        
        ));
        //generamos el regOperacion
        DB::table('regOperaciones')->delete(); // limpiamos el contenido de la tabla
        Regoperacion::create(array(
			'operacion_id' => '1',
			'user_id' => '1',  // se genero por defecto 3 y 4
			'registro_id' => '1',
			
	        
        ));
        
       
        
    }
}
