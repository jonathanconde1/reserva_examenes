<?php

/**
 * Agregamos un usuario nuevo a la base de datos.
 */
class CategoryTableSeeder extends Seeder {
    public function run(){
		DB::table('categorias')->delete();
        Category::create(array(
			'nombre' => 'AI',
	        'active' => '1',
            
        ));
        Category::create(array(
			'nombre' => 'AII',
	        'active' => '1',
            
        ));
        Category::create(array(
			'nombre' => 'AIII',
	        'active' => '1',
            
        ));
        
    }

}