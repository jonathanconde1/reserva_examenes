<?php

/**
 * Agregamos un usuario nuevo a la base de datos.
 */
class TipoTableSeeder extends Seeder {
    public function run(){
		DB::table('tipos')->delete();
        Tipo::create(array(
			'name' => 'ORIGINAL',
	        'status' => 1,
            
        ));
        Tipo::create(array(
            'name' => 'RECATEGORIZACIÓN',
            'status' => 1,
            
        ));
        
    }
}
