<?php

/**
 * Agregamos un usuario nuevo a la base de datos.
 */
class DetailCategoryTableSeeder extends Seeder {
    public function run(){
		DB::table('categorias_detalles')->delete();
        CategoryDetail::create(array(
			'name' => 'AI',
	        'active' => '1',
	        'categoria_id' => '1',
            
        ));
        CategoryDetail::create(array(
			'name' => 'AIIA',
	        'active' => '1',
	        'categoria_id' => '2',
            
        ));
        CategoryDetail::create(array(
			'name' => 'AIIB',
	        'active' => '1',
	        'categoria_id' => '2',
            
        ));
        CategoryDetail::create(array(
			'name' => 'AIIIA',
	        'active' => '1',
	        'categoria_id' => '3',
            
        ));
        CategoryDetail::create(array(
			'name' => 'AIIIB',
	        'active' => '1',
	        'categoria_id' => '3',
            
        ));
        CategoryDetail::create(array(
			'name' => 'AIIIC',
	        'active' => '1',
	        'categoria_id' => '3',
            
        ));
        
        
    }

}