<?php
/**
 * Agregamos un usuario nuevo a la base de datos.
 */
class OperacionTableSeeder extends Seeder {
    public function run(){
		DB::table('operaciones')->delete();
        Operacion::create(array(
			'name' => 'crear',
	        
        ));
        Operacion::create(array(
			'name' => 'editar',
	        
        ));
        Operacion::create(array(
			'name' => 'eliminar',
	    ));
	    
	    Operacion::create(array(
			'name' => 'mover',
	    ));
	    Operacion::create(array(
			'name' => 'activar',
	    ));
	    Operacion::create(array(
			'name' => 'desactivar',
	    ));

        
        
    }
}
