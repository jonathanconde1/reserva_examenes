<?php

/**
 * Agregamos llaves a suarios.
 */
class KeyTableSeeder extends Seeder {
    public function run(){
		DB::table('keys')->delete();
        Key::create(array(
			'code' => str_random(8),
	        'type' => 0, // 0 = privado , 1 = publico
	        'status' => 1, //0 = inactivo, 1 = Activo
            'user_id' => 1,
            
        ));
        Key::create(array(
            'code' => str_random(8),
            'type' => 0, // 0 = privado , 1 = publico
            'status' => 1, //0 = inactivo, 1 = Activo
            'user_id' => 2,
            
        ));
        Key::create(array(
            'code' => str_random(8),
            'type' => 1, // 0 = privado , 1 = publico
            'status' => 1, //0 = inhabilidato, 1 = Activo
            'user_id' => 1,
            
        ));
        Key::create(array(
            'code' => str_random(8),
            'type' => 1, // 0 = privado , 1 = publico
            'status' => 1, //0 = inhabilidato, 1 = Activo
            'user_id' => 1,
            
        ));
        
    }
       
    

}

