<?php

/**
 * Agregamos un usuario nuevo a la base de datos.
 */
class PersonaTableSeeder extends Seeder {
    public function run(){
		DB::table('personas')->delete();
        Persona::create(array(
			'dni' => '42500934',

	        'nombres' => 'Elí Jonathan', // Hash::make() nos va generar una cadena con nuestra contraseña encriptada
	        'apellidos' => 'Conde Maquera',            
            
        ));
        Persona::create(array(
			'dni' => '40970691',

	        'nombres' => 'Jean Carlos', // Hash::make() nos va generar una cadena con nuestra contraseña encriptada
	        'apellidos' => 'Villalba Aguas',
            
        ));
        Persona::create(array(
			'dni' => '42500935',

	        'nombres' => 'Gloria', // Hash::make() nos va generar una cadena con nuestra contraseña encriptada
	        'apellidos' => 'Quispe Condori',
            
        ));
        Persona::create(array(
			'dni' => '42500936',

	        'nombres' => 'Mariella', // Hash::make() nos va generar una cadena con nuestra contraseña encriptada
	        'apellidos' => 'Condori Quispe',
            
        ));

      
        
    }

}
