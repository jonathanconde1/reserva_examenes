  var yy;
  var calendarArray =[];
  var monthOffset = [6,7,8,9,10,11,0,1,2,3,4,5];
  var monthArray = [["ENE","enero"],["FEB","Febrero"],["MAR","Marzo"],["ABR","Abril"],["MAY","Mayo"],["JUN","Junio"],["JUL","Julio"],["AGO","Agosto"],["SEP","Septiembre"],["OCT","Octubre"],["NOV","Noviembre"],["DIC","Diciembre"]];
  var letrasArray = ["Domingo","Lunes","Martes","Miercoles","Jueves","Viernes","Sabado"];
  var dayArray = ["1","2","3","4","5","6","7"];
  
  $(document).ready(function() {
    setInterval('digiClock()', 1000);
    //cargando valores por defecto
    $("#categoriaDefault").html("RELACIÓN DE EXAMEN DE MANEJO CATEGORIA AI");
    $("#categoriaActual").val()//le asigno el id de la categoria

    //cargando detalle categoria ejem. AIIA, AIIB, etc.
    buscar_detalle_cat();

    

    //end cargar valores por defecto
    var categoriaActual = $("#categoriaActual").val();

    $(document).on('click','.calendar-day.have-events',activateDay); //ir a 
    

    $(document).on('click','.specific-day',activatecalendar); //regreso
    

    $(".cancelar").click(function(e){
      //e.preventDefault;
      //alert("SDDD");
      //activatecalendar();
      $("#formulario").hide();
      //$("#add_reg").hide();

      $("#calendario").show();
      cargar_mes_actual();
      $(".specific-day").click();
    });
    
    
    $(document).on('click','.calendar-month-view-arrow',offsetcalendar); // asignar mes
    $(document).on('click','.calendar-month-view-arrow',cargar_mes_actual); // asignar mes

    $(window).resize(calendarScale);
    

    $(".calendar").calendar({
      "2013910": {
        "Mulberry Festival": {
          start: "9.00",
          end: "9.30",
          location: "London"
        }
      }
    });
    
  
    calendarSet();
    calendarScale();
    //$("#data").html("<label>XDDDDD</label>");
    var mes_actual = $("#mes_actual").val();

    cargar_mes_actual();
    
    $("<div >"+mes_actual+"</div>").appendTo("#data");
    


    $(".have-events").click(function(e){
      //alert("XDDD");
     // var mes_actual = $("#mes_actual").val();
      //var dia_actual = $(this).val();
      
        //$("<div data-role='day' data-day='2015428'>"+mes_actual+"</div>").appendTo("#data");
        //$("<div data-role='day' data-day='2015428'>"+dia_actual+"</div>").appendTo("#data");


    });
    
    $(".agregar_registro").click(function(e){
      e.preventDefault();
      //alert("desea agregar registro ");
      $("#add_reg").show();
    });

    
    $("#agregarUsuario").click(function(e){
      
      var data_dni =$("#data-dni").val();
      var data_persona_id = $("#data-persona_id").val();
      var data_apellidos = $("#data-apellidos").val();
      var data_nombres = $("#data-nombres").val();
      var data_email = $("#data-email").val();
      var data_email_val = $("#data-email").data("value");

      var data_rol = $("#data-rol").val();
      var data_tramite = $("#data-tramite").val();
      var data_celular = $("#data-celular").val();
      var data_pass = $("#data-pass").val();

      //alert(data_dni+data_persona_id+data_apellidos+data_apellidos+data_nombres+data_email+data_rol+data_tramite+data_celular+data_pass+data_email_val);

      if(data_dni==""){
        alert("Se requiere DNI");
      }else if(data_apellidos==""){
        alert("Se requiere Apellidos");
      }else if(data_nombres==""){
        alert("Se requiere Nombres");
      }else if((data_email=="")||(data_email_val=="0")){
        alert("Se requiere email válido");
      }else if(data_tramite==""){
        alert("Se requiere trámite");
      }else if(data_celular==""){
        alert("Se requiere celular");
      }else if(data_pass==""){
        alert("Se requiere password");
      }else{
        //alert("Por fin puede agregar usuario");
        ingresarUsuario();

      }


    });
    $("#agregarRegistro").click(function(e){
      var data_dni =$("#data-dni").val();
      var data_persona_id = $("#data-persona_id").val();
      var data_apellidos = $("#data-apellidos").val();
      var data_nombres = $("#data-nombres").val();
      var data_llave = $("#data-llave").val();
      var cantidad_reg = $("#data_registro").data("cantidad");
      
      
      validarLlave();
  //alert("agregar reg");
      
      if(data_dni==""){
        $("#data-dni").focus();
        alert("No hay dni");
      }else if(data_apellidos==""){
        $("#data-apellidos").focus();
        alert("No hay apellidos");
      }else if(data_nombres==""){
        $("#data-nombres").focus();
        alert("No hay nombres");
      }else if((cantidad_reg>=15)){
         
         var llave_status = $("#codeResultado").data("llave");
      

        if(data_llave==""){
          alert("Se requiere llave ...");  
        }else if(llave_status=="0"){
          alert("Llave inválida ...");  

        }else{
          //alert("todo va bien con la llave :D" + cantidad_reg);  
          ingresarRegistro();
        }

        
      }else{
        //alert("todo va bien :D" + cantidad_reg);
        ingresarRegistro();
      }

      //
      

    });
    
    $("#data-email").focusout(function(e){
      
      validarEmail();

    });
    
    $("#data-dni").focusout(function(e){
      var dni = $("#data-dni").val();
      //estas en el apellidos
      if(dni == ""){
        alert("no hay datos en dni");
        //$("#data-dni").focus();
      }else{
        validarDni();
        //realizo la consulta
        var parametros = {
          "dni" : $('#data-dni').val()
        };
        $.post( "dni",parametros)
        .done(function( data ) {
          //alert( "Data Loaded: " + data );
          //var dni = ["123","234","345"];
          var data = data.split(", ");
          $("#datos").html("datos encontrados:<br>"+data[0]);
          $("#data-persona_id").val(data[0]);
          $("#data-apellidos").val(data[1]);
          $("#data-nombres").val(data[2]);
          $("#data-apellidos").attr("disabled","disabled");
          $("#data-nombres").attr("disabled","disabled");
          //asignar variable oculta para decidir si 
          //guardar datos nombre o solo el id

          $("#data-apellidos").html("<label>"+data[0]+"</label>");

        })
          .fail(function(data){
            //alert("evento fallido");
            $("#datos").html("no hay datos");
            $("#data-apellidos").val("");
            $("#data-nombres").val("");
            $("#data-persona_id").val("");    
            
        });
      }     
      
    });

    $("#data-dni").focusin(function(e){
      $("#data-apellidos").removeAttr("disabled");
      $("#data-nombres").removeAttr("disabled");
      
            
    });


    $("#categoria1").click(function(e){
      e.preventDefault();
      $("#categoriaActual").val("1"); //le asigno el id de la categoria
      $("#categoriaDefault").html("RELACIÓN DE EXAMEN DE MANEJO CATEGORIA AI");
      buscar_detalle_cat();
      cargar_mes_actual();
      verRegistros();

    });
    $("#categoria2").click(function(e){
      e.preventDefault();
      var categoriaActual = $("#categoriaActual").val();
      $("#categoriaActual").val("2"); //le asigno el id de la categoria
      $("#categoriaDefault").html("RELACIÓN DE EXAMEN DE MANEJO CATEGORIA AII");
      buscar_detalle_cat();
      cargar_mes_actual();
      verRegistros();

    });
    $("#categoria3").click(function(e){
      e.preventDefault();
      $("#categoriaActual").val("3"); //le asigno el id de la categoria
      $("#categoriaDefault").html("RELACIÓN DE EXAMEN DE MANEJO CATEGORIA AIII");
      buscar_detalle_cat();
      cargar_mes_actual();
      verRegistros();

    });
    
    $(".mover").click(function (e){
      
      $(".cancelar").click();

      $("#data-operacion").val("mover");

    });

    $(".eliminar").click(function (e){
      //alert("eliminar?");
      eliminarRegistro();
        //$(".cancelar").click();

      //$("#data-operacion").val("eliminar");

    });

    $(".exportar").click(function (e){
      //alert("intento exportar");
      $("#exportar").submit();
       /*
       var parametros = {
          "dia" : $('#data-fecha').val(),
          "mes" : $('#mes_actual').val(),
          "anio" : $('#anio_actual').val(),
          "categoria_id" : $('#categoriaActual').val()
          
        };
        //var mes = $('#categoriaActual').val();
        //alert("holasss"+mes);
        
    $.post( "exportarExcel",parametros)
        .done(function( data ) {
          alert("exito al exportar");
          
          

        })
          .fail(function(data){
            alert("Error al exportar Registros");
            //$("tr#reg").html("");   //limpiar registros         
        });
    */
    });


    $("#verificarCode").click(function(e){
      //alert("verificando");
      validarLlave();
    });

//desbloqueando camppos apellidos nombres

    /*
    $("#the-basics").keyup(function(e){
      //alert("");
       var parametros = {
          "dniData" : $('#data-dni').val()
        };
      //var dniData = $("#data-dni").val();

      $.post( "dni",parametros)
        .done(function( data ) {
          alert( "Data Loaded: " + data );
          //var dni = ["123","234","345"];

      })
        .fail(function(data){
          alert("evento fallido");
      });

    });
  */

  });

  
  function buscar_detalle_cat (){ //se encarga de cargar los detalles de categoria
    //alert("Buscando detalle");
    
    var parametros = {
          "categoria_id" : $('#categoriaActual').val()
          //"categoria_id" : "3"
        };
    $.post( "categoriaDetalle",parametros)
        .done(function( data ) {
          $("select#categoria").html("");
          var dato = $.parseJSON(data);
          //var dato = data.id4;
          //alert("primer parametro"+dato[0]);
          
          //var detalle = data.split(", ");
          
          //var data = data.split(", ");
          for (var i=0;i<dato.length;i++)
          {
            var detalle = dato[i];
            var detalle = detalle.split("-");
            $('<option value="'+detalle[0]+'">'+detalle[1]+'</option>').appendTo("select#categoria");

          }          

        })
          .fail(function(data){
            //alert("evento fallido");
            $("#datos").html("no hay datos");
            $("#data-apellidos").val("");
            $("#data-nombres").val("");    
            
        });
  
  }

  function cargar_mes_actual(){ // carga la cantidad de registros que hay en los dias del calendario
    var mes = $('#mes_actual').val();
    var parametros = {
          "mes" : $('#mes_actual').val(),
          "anio" : $('#anio_actual').val(),
          "categoria_id" : $('#categoriaActual').val()
          
        };
        //var mes = $('#categoriaActual').val();
        //alert(mes);
        
    $.post( "cantidadRegistros",parametros)
        .done(function( data ) {
          //alert("exito"+data);
          
          
          var dato = $.parseJSON(data);
          //detalle[0] :es la fecha del dia
          //detalle[1] : cantidad de registros encontrados
          //detalle[2] : porcentaje de 15
          //detalle[3] : id del grupo creado para ese dia
          //limpiar campos
          limpiarDias();
          //end limpiar campos

          for (var i=0;i<dato.length;i++)
          {
            var detalle = dato[i];
            var detalle = detalle.split("-");
            $("#dia"+detalle[0]+mes).attr("style","width: "+detalle[2]+"%");
            $("#dia"+detalle[0]+mes).data("group_id",detalle[3]);

            if(detalle[1]>14){
              $("#dia"+detalle[0]+mes).removeClass(); 
              $("#dia"+detalle[0]+mes).addClass('progress-bar progress-bar-danger');
            }else if(detalle[1]>11){
              $("#dia"+detalle[0]+mes).removeClass(); 
              $("#dia"+detalle[0]+mes).addClass('progress-bar progress-bar-info');
            }else{
              $("#dia"+detalle[0]+mes).removeClass(); 
              $("#dia"+detalle[0]+mes).addClass('progress-bar progress-bar-success');
            }

            //progress-bar progress-bar-info
          }


        })
          .fail(function(data){
            //alert("evento fallido");
           limpiarDias();         
            
            
        });
  
    
  }

  function limpiarDias(){
    var mes = $('#mes_actual').val();
      for (var i=0;i<35;i++)
          {
            
            $("#dia"+i+mes).attr("style","width: 0%");
            //$('<option value="'+detalle[0]+'">'+detalle[1]+'</option>').appendTo("select#categoria");


          }
  }

  function verRegistros(){
    exportarDatos();
    var parametros = {
          "dia" : $('#data-fecha').val(),
          "mes" : $('#mes_actual').val(),
          "anio" : $('#anio_actual').val(),
          "categoria_id" : $('#categoriaActual').val()
          
        };
        //var mes = $('#categoriaActual').val();
        //alert("holasss"+mes);
        
    $.post( "verRegistros",parametros)
        .done(function( data ) {
          //alert("exito"+data);
          $("tr#reg").html("");   //limpiar registros         

          if(data!="error"){ // error = no hay registros
            var dato = $.parseJSON(data);
            var cant_datos =0;
            //detalle[0] :es la fecha del dia
            //detalle[1] : cantidad de registros encontrados
            //detalle[2] : porcentaje de 15
            //detalle[3] : id del grupo creado para ese dia
            
            for (var i=0;i<dato.length;i++)
            {
              var detalle = dato[i];
              var numero = i+1;
              var detalle = detalle.split("-");

              if(detalle[9]=='1'){
                $("<tr id='reg' ><td>"+numero+"</td><td>"+detalle[0]+"</td><td>"+detalle[1]+"</td><td>"+detalle[2]+"</td><td>"+detalle[3]+"</td><td>"+detalle[4]+"</td><td>"+detalle[5]+"</td><td>"+detalle[6]+"</td><td>"+detalle[7]+"</td><td>"+detalle[8]+"</td><td><input id='registro_id"+i+"' type='checkbox' data-id='"+detalle[10]+"'></td></tr>").appendTo("#data_registro");  
              }else{
               $("<tr id='reg' ><td>"+numero+"</td><td>"+detalle[0]+"</td><td>"+detalle[1]+"</td><td>"+detalle[2]+"</td><td>"+detalle[3]+"</td><td>"+detalle[4]+"</td><td>"+detalle[5]+"</td><td>"+detalle[6]+"</td><td>"+detalle[7]+"</td><td>"+detalle[8]+"</td><td></td></tr>").appendTo("#data_registro");  
              }
              
              $("#data_registro").data("cantidad",dato.length);

              //$("#dia"+detalle[0]).attr("style","width: "+detalle[2]+"%");
              
              //alert(detalle[3]);
              //$('<option value="'+detalle[0]+'">'+detalle[1]+'</option>').appendTo("select#categoria");
            }            
            if($("#data_registro").data("cantidad")>=15){
              $("#llave").show();
            }else{
              $("#llave").hide();
            }

          }else{
            $("tr#reg").html("");            
          }
          
          

        })
          .fail(function(data){
            $("tr#reg").html("");   //limpiar registros 
            alert("Consulta fallida al controlador - Ver Registros");
            //$("tr#reg").html("");   //limpiar registros         
        });
    
  }

function ingresarRegistro(){
    /*
    var persona_id = $("#data-persona_id").val();
    var persona_dni = $("#data-dni").val();
    var persona_apellidos = $("#data-apellidos").val();
    var persona_nombres = $("#data-nombres").val();
    var dia = $('#data-fecha').val();
    var mes = $('#mes_actual').val(); //recordar que el mes empieza en 0 = enero
    var anio = $('#anio_actual').val();
    var categoria_id = $('#categoriaActual').val();

    var tipo_id = $("#opcion_tipo option:selected").val();
    var cat_detalle_id = $("#categoria option:selected").val();
    var observacion = $("#data-observacion").val();
    
    var cantidad_registros =  $("#data_registro").data("cantidad");
    
    //alert("tipo_id"+tipo_id+cat_detalle_id+observacion+cantidad_registros); //exito con todos los parametros 
    */

    var parametros = {
        "persona_id" : $("#data-persona_id").val(),
        "persona_dni" : $("#data-dni").val(),
        "persona_apellidos" : $("#data-apellidos").val(),
        "persona_nombres" : $("#data-nombres").val(),
        "dia" : $('#data-fecha').val(),
        "mes" : $('#mes_actual').val(),//recordar que el mes empieza en 0 = enero
        "anio" : $('#anio_actual').val(),
        "categoria_id" : $('#categoriaActual').val(),
        "tipo_id" : $("#opcion_tipo option:selected").val(),
        "cat_detalle_id" : $("#categoria option:selected").val(),
        "observacion" : $("#data-observacion").val(),

          "dia" : $('#data-fecha').val(),
          "mes" : $('#mes_actual').val(),
          "anio" : $('#anio_actual').val(),
        "code" : $("#data-code").val(),
          
          
    };

    

    $.post( "ingresarRegistro",parametros)
        .done(function( data ) {
          //alert("exito"+data);
          $("#data-dni").val("");
          $("#data-apellidos").val("");
          $("#data-nombres").val("");
          $("#observacion").val("");
          $("#codeResultado").html("");
          $("#data-llave").val("");
          $("#data-observacion").val("");
          

          verRegistros();

        })
          .fail(function(data){
            //alert(data);
            alert("Error al Ingresar Registro");
            //$("tr#reg").html("");   //limpiar registros         
        });


    
};

function ingresarUsuario(){

   var parametros = {

      "data_dni" : $("#data-dni").val(),
      "data_persona_id" : $("#data-persona_id").val(),
      "data_apellidos" : $("#data-apellidos").val(),
      "data_nombres" : $("#data-nombres").val(),
      "data_email" : $("#data-email").val(),
      "data_email_val" : $("#data-email").data("value"),

      "data_rol" : $("#data-rol").val(),
      "data_tramite" : $("#data-tramite").val(),
      "data_celular" : $("#data-celular").val(),
      "data_pass" : $("#data-pass").val(),
          
    };

    $.post( "ingresarUsuario",parametros)
        .done(function( data ) {
          alert("Usuario creado ... ! (En espera de ser aprobado por el administrador)");
          $("#data-dni").val("");
          $("#data-apellidos").val("");
          $("#data-nombres").val("");
          $("#data-email").val("");
          $("#data-email").data("value","0");

          $("#data-tramite").val("");
          $("#data-celular").val("");
          $("#data-pass").val("");

          //verRegistros();

        })
          .fail(function(data){
            //alert(data);
            alert("Error al Ingresar Usuario");
            //$("tr#reg").html("");   //limpiar registros         
        });


    
}

function validarLlave(){
  
  var parametros = {
        "code" : $("#data-code").val(),
          
    };

    $.post( "validarLlave",parametros)
        .done(function( data ) {
          $("#codeResultado").html("");
          
          //alert("exito = "+data);
          if(data!="error"){
            $("#codeResultado").html(data);
            $("#codeResultado").data("llave","1");

          }else{
            $("#codeResultado").html("Llave inválida ...");
            $("#codeResultado").data("llave","0");

          }

        })
          .fail(function(data){
            //alert(data);
            alert("Error al validar llave");
            //$("tr#reg").html("");   //limpiar registros         
        });


}


function validarDni(){
    //alert("Procesando dni");
    var dni = $("#data-dni").val();
    var resultado = "";

    var parametros = {
          "dia" : $('#data-fecha').val(),
          "mes" : $('#mes_actual').val(),
          "anio" : $('#anio_actual').val(),
          "categoria_id" : $('#categoriaActual').val()
          
        };
        //var mes = $('#categoriaActual').val();
        //alert(mes);
        
    $.post( "verRegistros",parametros)
        .done(function( data ) {
          //alert("exito"+data);
          
          if(data!="error"){
            var dato = $.parseJSON(data);
            
            //detalle[0] :es la fecha del dia
            //detalle[1] : cantidad de registros encontrados
            //detalle[2] : porcentaje de 15
            //detalle[3] : id del grupo creado para ese dia
            
            for (var i=0;i<dato.length;i++)
            {
              var detalle = dato[i];
              var detalle = detalle.split("-");

              //alert("dni = "+detalle[0]);

                if(detalle[0]==dni){
                  resultado = "EL dni ya esta registrado para este dia.";
                }else{
                  //resultado = "No Inscrito";
                }


            }
            if(resultado!=""){
              alert(resultado);
            }
                       

          }
          
          

        })
          .fail(function(data){
            alert("Error al validar DNI");
            //$("tr#reg").html("");   //limpiar registros   
            //alert("El DNI aun no exite de validar DNI");      
        });
    
  }
  //moverRegistro
  var mover = function (){
    alert("Moviendo Registro");

    $('input[type=checkbox]').each(function () {
      

      if (this.checked) {
        var dia = $('#data-fecha').val();
        var mes = $('#mes_actual').val();
        var anio = $('#anio_actual').val();
        var categ = $('#categoriaActual').val();

        var id_registro = $(this).data("id");
        
        //alert("id = "+id_registro+" : "+dia+"-"+mes+"-"+anio+" categ = "+categ);

        var parametros = {
          "dia" : $('#data-fecha').val(),
          "mes" : $('#mes_actual').val(),
          "anio" : $('#anio_actual').val(),
          "categoria_id" : $('#categoriaActual').val(),
          
          "registro_id" : $(this).data("id")
          
        };           
                
        $.post("moverRegistro",parametros)
          .done(function( data ) {
            //alert("Moviendo Registro");
            verRegistros();
              
                  

        })
          .fail(function(data){
          
            alert("Error al mover Registro");      
        });
               
      }

      
    });
    //al finalizar la operacion se debe borrar el evento mover

    $("#data-operacion").val(""); // borramos el estado mover registro
    
  
  }
  //antes verificarGrupo
  var verificar = function (){
    //alert("Verificando");

       //alert("XD"+$('#data-fecha').val()+$('#mes_actual').val()+$('#anio_actual').val()+$('#categoriaActual').val());
        var parametros = {
          "dia" : $('#data-fecha').val(),
          "mes" : $('#mes_actual').val(),
          "anio" : $('#anio_actual').val(),
          "categoria_id" : $('#categoriaActual').val()
                    
        };           
                
        $.post("verificarGrupo",parametros)
          .done(function( data ) {
            //alert("Verificando, exito id grupo = "+data);
            //verRegistros();
              
        })
          .fail(function(data){
          alert("Error al ferificar el grupo");      
        });

  }

  function eliminarRegistro(){
    

    $('input[type=checkbox]').each(function () {
      

      if (this.checked) {
        var dia = $('#data-fecha').val();
        var mes = $('#mes_actual').val();
        var anio = $('#anio_actual').val();
        var categ = $('#categoriaActual').val();

        var id_registro = $(this).data("id");
        
        //alert("id = "+id_registro+" : "+dia+"-"+mes+"-"+anio+" categ = "+categ);

        var parametros = {
          "dia" : $('#data-fecha').val(),
          "mes" : $('#mes_actual').val(),
          "anio" : $('#anio_actual').val(),
          "categoria_id" : $('#categoriaActual').val(),
          
          "registro_id" : $(this).data("id")
          
        };           
                
        $.post("eliminarRegistro",parametros)
          .done(function( data ) {
            //alert("exito"+data);
            verRegistros();
              
                  

        })
          .fail(function(data){
          
            alert("Error al eliminar Registro");      
        });
               
      }

      
    });
    //al finalizar la operacion se debe borrar el evento mover

    $("#data-operacion").val("");
    
    
    //verRegistros();
  
  }

  function validarEmail(){
    var email = $('#data-email').val();

    if(email!=""){
      var parametros = {
          "email" : $('#data-email').val()
          
          
        };           
                
        $.post("validarEmail",parametros)
          .done(function( data ) {
            //alert("exito"+data);
            if(data==1){
              $("#info_email").html("Email Inválido");
              $("#data-email-val").data("value","0");
            }else{
              $("#info_email").html("Email válido");
              $("#data-email").data("value","1");
            }
            
        })
          .fail(function(data){
          
            alert("Error al realizar la consulta de Email...");      
        });  
    }


  }

  function exportarDatos(){

    $("#exporta_dia").val($('#data-fecha').val());
    $("#exporta_dia_id").val($('#data-fecha-id').val());
    $("#exporta_mes").val($('#mes_actual').val());
    $("#exporta_anio").val($('#anio_actual').val());
    $("#exporta_categoria_id").val($('#categoriaActual').val());
          
      
  }

  function digiClock ( )  {
    var crTime = new Date ( );
    var crHrs = crTime.getHours ( );
    var crMns = crTime.getMinutes ( );
    var crScs = crTime.getSeconds ( );
    crMns = ( crMns < 10 ? "0" : "" ) + crMns;
    crScs = ( crScs < 10 ? "0" : "" ) + crScs;
    var timeOfDay = ( crHrs < 12 ) ? "AM" : "PM";
    crHrs = ( crHrs > 12 ) ? crHrs - 12 : crHrs;
    crHrs = ( crHrs == 0 ) ? 12 : crHrs;
    var crTimeString = crHrs + ":" + crMns + ":" + crScs + " " + timeOfDay;
 
    $("#clock").html(crTimeString);
 
 }

var fun1 = function (){
  alert("funcion1");
}
var fun2 = function (){
  alert("funcion2");
}

