  $(document).ready(function() {

    setInterval('digiClock()', 1000);
    //cargando valores por defecto
    verUsuarios();
    $("#nuevo_usuario").hide();

    $(".cancelar").click(function(e){
      
    });
    
    
    $(".crear").click(function(e){
      e.preventDefault();
      //alert("desea agregar registro ");
      //$("#add_reg").show();
      $("#tabla_usuarios").hide();

      $("#nuevo_usuario").show();
      //alert("crear");
    });

    $("#cancelarRegistro").click(function(e){
		//$("#tabla_usuarios").show();
    	$("#tabla_usuarios").show();
    	$("#nuevo_usuario").hide();

    });

    $("#agregarRegistro").click(function(e){
      var data_dni =$("#data-dni").val();
      var data_persona_id = $("#data-persona_id").val();
      var data_apellidos = $("#data-apellidos").val();
      var data_nombres = $("#data-nombres").val();
      var data_llave = $("#data-llave").val();
      var cantidad_reg = $("#data_registro").data("cantidad");
      validarLlave();
  
      
      if(data_dni==""){
        $("#data-dni").focus();
        alert("No hay dni");
      }else if(data_apellidos==""){
        $("#data-apellidos").focus();
        alert("No hay apellidos");
      }else if(data_nombres==""){
        $("#data-nombres").focus();
        alert("No hay nombres");
      }else if((cantidad_reg>=15)){
         
         var llave_status = $("#codeResultado").data("llave");
      

        if(data_llave==""){
          alert("Se requiere llave ...");  
        }else if(llave_status=="0"){
          alert("Llave inválida ...");  

        }else{
          //alert("todo va bien :D" + cantidad_reg);  
          ingresarRegistro();
        }

        
      }else{
        //alert("todo va bien :D" + cantidad_reg);
        ingresarRegistro();
      }

      //
      

    });
    
    
    $("#data-dni").focusout(function(e){
      var dni = $("#data-dni").val();
      //estas en el apellidos
      if(dni == ""){
        alert("no hay datos en dni");
        //$("#data-dni").focus();
      }else{
        validarDni();
        //realizo la consulta
        var parametros = {
          "dni" : $('#data-dni').val()
        };
        $.post( "dni",parametros)
        .done(function( data ) {
          //alert( "Data Loaded: " + data );
          //var dni = ["123","234","345"];
          var data = data.split(", ");
          $("#datos").html("datos encontrados:<br>"+data[0]);
          $("#data-persona_id").val(data[0]);
          $("#data-apellidos").val(data[1]);
          $("#data-nombres").val(data[2]);
          $("#data-apellidos").attr("disabled","disabled");
          $("#data-nombres").attr("disabled","disabled");
          //asignar variable oculta para decidir si 
          //guardar datos nombre o solo el id

          $("#data-apellidos").html("<label>"+data[0]+"</label>");

        })
          .fail(function(data){
            //alert("evento fallido");
            $("#datos").html("no hay datos");
            $("#data-apellidos").val("");
            $("#data-nombres").val("");
            $("#data-persona_id").val("");    
            
        });
      }     
      
    });

    $("#data-dni").focusin(function(e){
      $("#data-apellidos").removeAttr("disabled");
      $("#data-nombres").removeAttr("disabled");
      
            
    });


    
    $(".mover").click(function (e){
      
      $(".cancelar").click();

      $("#data-operacion").val("mover");

    });

    $(".eliminar").click(function (e){
      //alert("eliminar?");
      eliminarRegistro();
        //$(".cancelar").click();

      //$("#data-operacion").val("eliminar");

    });

    $("#verificarCode").click(function(e){
      //alert("verificando");
      validarLlave();
    });

//desbloqueando camppos apellidos nombres

    /*
    $("#the-basics").keyup(function(e){
      //alert("");
       var parametros = {
          "dniData" : $('#data-dni').val()
        };
      //var dniData = $("#data-dni").val();

      $.post( "dni",parametros)
        .done(function( data ) {
          alert( "Data Loaded: " + data );
          //var dni = ["123","234","345"];

      })
        .fail(function(data){
          alert("evento fallido");
      });

    });
  */

  });

  
  function buscar_detalle_cat (){ //se encarga de cargar los detalles de categoria
    //alert("Buscando detalle");
    
    var parametros = {
          "categoria_id" : $('#categoriaActual').val()
          //"categoria_id" : "3"
        };
    $.post( "categoriaDetalle",parametros)
        .done(function( data ) {
          $("select#categoria").html("");
          var dato = $.parseJSON(data);
          //var dato = data.id4;
          //alert("primer parametro"+dato[0]);
          
          //var detalle = data.split(", ");
          
          //var data = data.split(", ");
          for (var i=0;i<dato.length;i++)
          {
            var detalle = dato[i];
            var detalle = detalle.split("-");
            $('<option value="'+detalle[0]+'">'+detalle[1]+'</option>').appendTo("select#categoria");

          }          

        })
          .fail(function(data){
            //alert("evento fallido");
            $("#datos").html("no hay datos");
            $("#data-apellidos").val("");
            $("#data-nombres").val("");    
            
        });
  
  }

  function cargar_mes_actual(){ // carga la cantidad de registros que hay en los dias del calendario
    var parametros = {
          "mes" : $('#mes_actual').val(),
          "anio" : $('#anio_actual').val(),
          "categoria_id" : $('#categoriaActual').val()
          
        };
        //var mes = $('#categoriaActual').val();
        //alert(mes);
        
    $.post( "cantidadRegistros",parametros)
        .done(function( data ) {
          //alert("exito"+data);
          
          
          var dato = $.parseJSON(data);
          //detalle[0] :es la fecha del dia
          //detalle[1] : cantidad de registros encontrados
          //detalle[2] : porcentaje de 15
          //detalle[3] : id del grupo creado para ese dia
          //limpiar campos
          limpiarDias();
          //end limpiar campos

          for (var i=0;i<dato.length;i++)
          {
            var detalle = dato[i];
            var detalle = detalle.split("-");
            $("#dia"+detalle[0]).attr("style","width: "+detalle[2]+"%");
            $("#dia"+detalle[0]).data("group_id",detalle[3]);
            //alert(detalle[3]);
            //$('<option value="'+detalle[0]+'">'+detalle[1]+'</option>').appendTo("select#categoria");


          }          
          

        })
          .fail(function(data){
            //alert("evento fallido");
           limpiarDias();         
            
            
        });
  
    
  }

  function limpiarDias(){
      for (var i=0;i<35;i++)
          {
            
            $("#dia"+i).attr("style","width: 0%");
            //$('<option value="'+detalle[0]+'">'+detalle[1]+'</option>').appendTo("select#categoria");


          }
  }

  function verUsuarios(){

    $.post("verUsuario")
        .done(function( data ) {
          //alert("exito"+data);
          $("tr#reg").html("");   //limpiar registros         
          
          if(data!="error"){
            var dato = $.parseJSON(data);
            var cant_datos =0;
            //detalle[0] :es la fecha del dia
            //detalle[1] : cantidad de registros encontrados
            //detalle[2] : porcentaje de 15
            //detalle[3] : id del grupo creado para ese dia
            
            for (var i=0;i<dato.length;i++)
            {
              var detalle = dato[i];
              var numero = i+1;
              var detalle = detalle.split("-");

              if(detalle[5]=='Por Activar'){
                $("<tr id='reg' ><td>"+numero+"</td><td>"+detalle[0]+"</td><td>"+detalle[1]+"</td><td>"+detalle[2]+"</td><td>"+detalle[3]+"</td><td>"+detalle[4]+"</td><td>"+detalle[5]+"</td><td><a href='eliminarUsuario/"+detalle[6]+"'>Eliminar</a> / <a href='activarUsuario/"+detalle[6]+"'>Activar</a></td></tr>").appendTo("#data_registro");
              }else{
                $("<tr id='reg' ><td>"+numero+"</td><td>"+detalle[0]+"</td><td>"+detalle[1]+"</td><td>"+detalle[2]+"</td><td>"+detalle[3]+"</td><td>"+detalle[4]+"</td><td>"+detalle[5]+"</td><td><a href='eliminarUsuario/"+detalle[6]+"'>Eliminar</a> / <a href='desactivarUsuario/"+detalle[6]+"'>Deshabilitar</a></td></tr>").appendTo("#data_registro");
              }
              
              $("#data_registro").data("cantidad",dato.length);


            }            
           

          }else{
            $("tr#reg").html("");
          }
          
          

        })
          .fail(function(data){
            //alert("evento fallido");
            $("tr#reg").html("");   //limpiar registros         
        });
  
  }

function ingresarRegistro(){
    /*
    var persona_id = $("#data-persona_id").val();
    var persona_dni = $("#data-dni").val();
    var persona_apellidos = $("#data-apellidos").val();
    var persona_nombres = $("#data-nombres").val();
    var dia = $('#data-fecha').val();
    var mes = $('#mes_actual').val(); //recordar que el mes empieza en 0 = enero
    var anio = $('#anio_actual').val();
    var categoria_id = $('#categoriaActual').val();

    var tipo_id = $("#opcion_tipo option:selected").val();
    var cat_detalle_id = $("#categoria option:selected").val();
    var observacion = $("#data-observacion").val();
    
    var cantidad_registros =  $("#data_registro").data("cantidad");
    alert(dia+mes+anio); //exito con todos los parametros 
    */

    var parametros = {
        "persona_id" : $("#data-persona_id").val(),
        "persona_dni" : $("#data-dni").val(),
        "persona_apellidos" : $("#data-apellidos").val(),
        "persona_nombres" : $("#data-nombres").val(),
        "dia" : $('#data-fecha').val(),
        "mes" : $('#mes_actual').val(),//recordar que el mes empieza en 0 = enero
        "anio" : $('#anio_actual').val(),
        "categoria_id" : $('#categoriaActual').val(),
        "tipo_id" : $("#opcion_tipo option:selected").val(),
        "cat_detalle_id" : $("#categoria option:selected").val(),
        "observacion" : $("#data-observacion").val(),

          "dia" : $('#data-fecha').val(),
          "mes" : $('#mes_actual').val(),
          "anio" : $('#anio_actual').val(),
        "code" : $("#data-code").val(),
          
          
    };

    

    $.post( "ingresarRegistro",parametros)
        .done(function( data ) {
          //alert("exito"+data);
          $("#data-dni").val("");
          $("#data-apellidos").val("");
          $("#data-nombres").val("");
          $("#observacion").val("");
          $("#codeResultado").html("");
          $("#data-llave").html("");
          

          verRegistros();

        })
          .fail(function(data){
            alert(data);
            alert("evento fallido");
            //$("tr#reg").html("");   //limpiar registros         
        });


    
};

function validarLlave(){
  
  var parametros = {
        "code" : $("#data-code").val(),
          
    };

    $.post( "validarLlave",parametros)
        .done(function( data ) {
          $("#codeResultado").html("");
          
          //alert("exito = "+data);
          if(data!="error"){
            $("#codeResultado").html(data);
            $("#codeResultado").data("llave","1");

          }else{
            $("#codeResultado").html("Llave inválida ...");
            $("#codeResultado").data("llave","0");

          }

        })
          .fail(function(data){
            //alert(data);
            alert("evento fallido");
            //$("tr#reg").html("");   //limpiar registros         
        });


}


function validarDni(){
    //alert("Procesando dni");
    var dni = $("#data-dni").val();
    var resultado = "";

    var parametros = {
          "dia" : $('#data-fecha').val(),
          "mes" : $('#mes_actual').val(),
          "anio" : $('#anio_actual').val(),
          "categoria_id" : $('#categoriaActual').val()
          
        };
        //var mes = $('#categoriaActual').val();
        //alert(mes);
        
    $.post( "verRegistros",parametros)
        .done(function( data ) {
          //alert("exito"+data);
          
          if(data!="error"){
            var dato = $.parseJSON(data);
            
            //detalle[0] :es la fecha del dia
            //detalle[1] : cantidad de registros encontrados
            //detalle[2] : porcentaje de 15
            //detalle[3] : id del grupo creado para ese dia
            
            for (var i=0;i<dato.length;i++)
            {
              var detalle = dato[i];
              var detalle = detalle.split("-");

              //alert("dni = "+detalle[0]);

                if(detalle[0]==dni){
                  resultado = "EL dni ya esta registrado para este dia.";
                }else{
                  //resultado = "No Inscrito";
                }


            }
            if(resultado!=""){
              alert(resultado);
            }
                       

          }
          
          

        })
          .fail(function(data){
            //alert("evento fallido");
            //$("tr#reg").html("");   //limpiar registros   
            //alert("El DNI aun no exite de validar DNI");      
        });
    
  }

  function moverRegistro(){
    

    $('input[type=checkbox]').each(function () {
      

      if (this.checked) {
        var dia = $('#data-fecha').val();
        var mes = $('#mes_actual').val();
        var anio = $('#anio_actual').val();
        var categ = $('#categoriaActual').val();

        var id_registro = $(this).data("id");
        
        alert("id = "+id_registro+" : "+dia+"-"+mes+"-"+anio+" categ = "+categ);

        var parametros = {
          "dia" : $('#data-fecha').val(),
          "mes" : $('#mes_actual').val(),
          "anio" : $('#anio_actual').val(),
          "categoria_id" : $('#categoriaActual').val(),
          
          "registro_id" : $(this).data("id")
          
        };           
                
        $.post("moverRegistro",parametros)
          .done(function( data ) {
            alert("exito id grupo = "+data);
            verRegistros();
              
                  

        })
          .fail(function(data){
          
            alert("eveto fallido");      
        });
               
      }

      
    });
    //al finalizar la operacion se debe borrar el evento mover

    $("#data-operacion").val("");
    
    
    //verRegistros();
  
  }
  function eliminarRegistro(){
    

    $('input[type=checkbox]').each(function () {
      

      if (this.checked) {
        var dia = $('#data-fecha').val();
        var mes = $('#mes_actual').val();
        var anio = $('#anio_actual').val();
        var categ = $('#categoriaActual').val();

        var id_registro = $(this).data("id");
        
        //alert("id = "+id_registro+" : "+dia+"-"+mes+"-"+anio+" categ = "+categ);

        var parametros = {
          "dia" : $('#data-fecha').val(),
          "mes" : $('#mes_actual').val(),
          "anio" : $('#anio_actual').val(),
          "categoria_id" : $('#categoriaActual').val(),
          
          "registro_id" : $(this).data("id")
          
        };           
                
        $.post("eliminarRegistro",parametros)
          .done(function( data ) {
            //alert("exito"+data);
            verRegistros();
              
                  

        })
          .fail(function(data){
          
            alert("eveto fallido");      
        });
               
      }

      
    });
    //al finalizar la operacion se debe borrar el evento mover

    $("#data-operacion").val("");
    
   
  }

  function buscarEmail(){ //busca email descartivados por dni
    

  }
  function validarEmail(){ // busca si ya existe

  }

  function digiClock ( )  {
    var crTime = new Date ( );
    var crHrs = crTime.getHours ( );
    var crMns = crTime.getMinutes ( );
    var crScs = crTime.getSeconds ( );
    crMns = ( crMns < 10 ? "0" : "" ) + crMns;
    crScs = ( crScs < 10 ? "0" : "" ) + crScs;
    var timeOfDay = ( crHrs < 12 ) ? "AM" : "PM";
    crHrs = ( crHrs > 12 ) ? crHrs - 12 : crHrs;
    crHrs = ( crHrs == 0 ) ? 12 : crHrs;
    var crTimeString = crHrs + ":" + crMns + ":" + crScs + " " + timeOfDay;
 
    $("#clock").html(crTimeString);
 
 }
